//
//  MarketData.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 28/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct MarketResponse {
    
    let event: String
    let topic: String
    let marketData: [MarketData]
    
    init(_ json: JSON) {
        event = json[ApiKey.event].stringValue
        topic = json[ApiKey.topic].stringValue
        marketData = MarketData.returnMarketDataListingArray(json[ApiKey.data])
    }
}

struct MarketData {
    
    let Id: String
    let CoinName: String
    let CurrentTradingPrice: Double // in use
    let TotalVolume: Double
    let ChangeInPrice: Double // in use
    let PreviousTradingPrice: Double
    let MarketName: String
    let Rank: Int
    let Status: Bool
    let CoinFullName: String
    let TotalVolumeBaseCurrency: Double
    let Last24HrsLow: Double
    let Last24HrsHigh: Double
    let MinTradeAmount: Double
    let MinTickSize: Double
    let MinOrderValue: Double
    
    init(_ json: JSON) {
        Id = json[ApiKey.Id].stringValue
        CoinName = json[ApiKey.CoinName].stringValue
        CurrentTradingPrice = json[ApiKey.CurrentTradingPrice].doubleValue
        TotalVolume = json[ApiKey.TotalVolume].doubleValue
        ChangeInPrice = json[ApiKey.ChangeInPrice].doubleValue
        PreviousTradingPrice = json[ApiKey.PreviousTradingPrice].doubleValue
        MarketName = json[ApiKey.MarketName].stringValue
        Rank = json[ApiKey.Rank].intValue
        Status = json[ApiKey.Status].boolValue
        CoinFullName = json[ApiKey.CoinFullName].stringValue
        TotalVolumeBaseCurrency = json[ApiKey.TotalVolumeBaseCurrency].doubleValue
        Last24HrsLow = json[ApiKey.Last24HrsLow].doubleValue
        Last24HrsHigh = json[ApiKey.Last24HrsHigh].doubleValue
        MinTradeAmount = json[ApiKey.MinTradeAmount].doubleValue
        MinTickSize = json[ApiKey.MinTickSize].doubleValue
        MinOrderValue = json[ApiKey.MinOrderValue].doubleValue
    }
    
    static func returnMarketDataListingArray(_ json: JSON) -> [MarketData] {
        return json.arrayValue.map{MarketData($0)}
    }
}
