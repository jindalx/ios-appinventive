//
//  UserPortfolio.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 28/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct UserPortofolio {
    var balance: Double
    var value: Double
    var swayePrice: Double = 0.0
    var changeInPrice: Double = 0.0
    var artist: UserArtist
    
    init(_ json: JSON) {
        balance = json[ApiKey.balance].doubleValue
        value = json[ApiKey.value].doubleValue
        artist = UserArtist(json[ApiKey.artist])
    }
    
    static func returnUserPortofolioListingArray(_ json: JSON) -> [UserPortofolio] {
        return json.arrayValue.map{UserPortofolio($0)}
    }
}

struct UserArtist {
    let id: String
    let name: String
    let symbol: String
    let img: String
    
    init(_ json: JSON) {
        id = json[ApiKey.id].stringValue
        name = json[ApiKey.name].stringValue
        symbol = json[ApiKey.symbol].string ?? "ARIG"
        img = json[ApiKey.img].stringValue
    }
}
