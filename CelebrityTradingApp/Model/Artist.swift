//
//  Artist.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct Artist {
    
    
    var id: String
    var name: String
    var symbol: String
    var img: String
    let tradeStats: TradeStats
    let tradeSettings: TradeSettings
    let recentNews: RecentNews
    var swayePrice: Double = 0.0
    var changeInPrice: Double = 0.0
    
    init() {
        id = ""
        name = ""
        symbol = "ARIG"
        img = ""
        tradeStats = TradeStats()
        tradeSettings = TradeSettings()
        recentNews = RecentNews()
    }
    
    init(_ json: JSON) {
        id = json[ApiKey.id].stringValue
        name = json[ApiKey.name].stringValue
        symbol = json[ApiKey.symbol].string ?? "ARIG"
        img = json[ApiKey.img].stringValue
        tradeStats = TradeStats(json[ApiKey.tradeStats])
        recentNews = RecentNews(json[ApiKey.recentNews])
        tradeSettings = TradeSettings(json[ApiKey.tradeSettings])
    }
    
    static func returnArtistsListingArray(_ json: JSON) -> [Artist] {
        return json.arrayValue.map{Artist($0)}
    }
}


struct RecentNews {
    var id: String = ""
    var artist: String = ""
    
    init() {}
    init(_ json: JSON) {
        id = json[ApiKey.id].stringValue
        artist = json[ApiKey.artist].stringValue
    }
}

struct TradeSettings {
    var buyEnabled: Bool = false
    var sellEnabled: Bool = false
    
    init() {}
    init(_ json: JSON) {
        buyEnabled = json[ApiKey.buyEnabled].boolValue
        sellEnabled = json[ApiKey.sellEnabled].boolValue
    }
}

struct TradeStats {
    var sparkline: [String] = [String]()
    
    init() {}
    init(_ json: JSON) {
        sparkline = json[ApiKey.sparkline].arrayValue.map({$0.stringValue})
    }
}
