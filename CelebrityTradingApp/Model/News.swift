//
//  News.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct News {
    
    var id: String
    var __typename: String
    var publishedTime : String
    
    init(json: JSON = JSON()) {
        self.id = json["id"].stringValue
        self.__typename = json["__typename"].stringValue
        publishedTime = json["publishedTime"].stringValue
    }
    
    static func returnNewsListingArray(_ json: JSON) -> [News] {
        return json.arrayValue.map{News(json: $0)}
    }

}
