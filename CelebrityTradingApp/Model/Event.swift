//
//  Event.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 30/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct Event {
    
    let id: String
    let name: String
    let date: String
    let location: String
    let url: String
    
    init(_ json: JSON) {
        id = json[ApiKey.id].stringValue
        name = json[ApiKey.name].stringValue
        date = json[ApiKey.date].stringValue
        location = json[ApiKey.location].stringValue
        url = json[ApiKey.url].stringValue
    }
    
    static func returnEventsListingArray(_ json: JSON) -> [Event] {
        return json.arrayValue.map{Event($0)}
    }
}

