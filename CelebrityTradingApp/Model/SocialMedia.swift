//
//  SocialMedia.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 30/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct SocialMedia {
    
    let id: String
    let artist: UserArtist
    let publishedTime: String
    let content: String
    let thumbnail: String
    let postUrl: String
    
    init(_ json: JSON) {
        id = json[ApiKey.id].stringValue
        publishedTime = json[ApiKey.publishedTime].stringValue
        content = json[ApiKey.content].stringValue
        thumbnail = json[ApiKey.thumbnail].stringValue
        postUrl = json[ApiKey.postUrl].stringValue
        artist = UserArtist(json[ApiKey.artist])
    }
    
    static func returnSocialListingArray(_ json: JSON) -> [SocialMedia] {
        return json.arrayValue.map({SocialMedia($0)})
    }
}
