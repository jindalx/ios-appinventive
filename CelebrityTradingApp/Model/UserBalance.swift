//
//  UserBalance.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 27/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct UserBalance {
    
    let currency: String
    let balance: String
    
    
    init(_ json: JSON) {
        currency = json[ApiKey.currency].stringValue
        balance = String(json[ApiKey.balance].doubleValue)
    }
    
    static func returnBalanceListingArray(_ json: JSON) -> [UserBalance] {
        return json.arrayValue.map{UserBalance($0)}
    }
    
}

