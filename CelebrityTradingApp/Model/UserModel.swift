//
//  UserModel.swift
//  CelebrityTradingApp
//
//  Created by Vikash on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

struct UserModel {
    
    static var main = UserModel(AppUserDefaults.value(forKey: .fullUserProfile)) {
         didSet {
             main.saveToUserDefaults(dictionaryToSave: UserModel.main.getDictionary)
         }
     }
    
    let id: Int
    let name: String
    var accessToken: String
    
    init(_ json: JSON) {
        self.id = json[ApiKey.userId].intValue
        self.name = json[ApiKey.name].stringValue
        self.accessToken = json[ApiKey.Authorization].stringValue
    }
    
    var getDictionary: JSONDictionary {
         
         var details = [String:Any]()
         details[ApiKey.userId] = id
         details[ApiKey.name] = name
         AppUserDefaults.save(value: details, forKey: .userData)
         return details
    }
    
    func saveToUserDefaults(dictionaryToSave: JSONDictionary) {
        AppUserDefaults.save(value: dictionaryToSave, forKey: .fullUserProfile)
    }
}
