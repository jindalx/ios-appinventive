//
//  ForYouHeaderFooterView.swift
//  CelebrityTradingApp
//
//  Created by Vishwesh on 07/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class ForYouHeaderFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var viewAll: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewAll.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        viewAll.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        viewAll.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    }
    
    func populateData(title: String) {
        headerTitleLabel.text  = title
    }
     
}
