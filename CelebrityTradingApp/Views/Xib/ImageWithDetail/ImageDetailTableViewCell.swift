//
//  ImageDetailTableViewCell.swift
//  CelebrityTradingApp
//
//  Created by Vishwesh on 07/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class ImageDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var cardCollectionView: UICollectionView!
    
    var currentSection = 0
    
    private var itemWidth: CGFloat = 0.0
    private var currentItem: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.cardCollectionView.registerCell(with: ImageDetailCollectionViewCell.self)
        self.cardCollectionView.registerCell(with: ImageWithTItleCollectionViewCell.self)
        self.cardCollectionView.delegate = self
        self.cardCollectionView.dataSource = self
        self.flowLayoutSetup()
    }
    
    func flowLayoutSetup() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        itemWidth = screen_width - 35
        layout.scrollDirection = .horizontal
        cardCollectionView.isPagingEnabled = false
        cardCollectionView.collectionViewLayout = layout
        cardCollectionView?.decelerationRate = UIScrollView.DecelerationRate.normal
    }

    
}

extension ImageDetailTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if currentSection == 1 {
            let cell = collectionView.dequeueCell(with: ImageWithTItleCollectionViewCell.self, indexPath: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueCell(with: ImageDetailCollectionViewCell.self, indexPath: indexPath)
            cell.populateData(section: self.currentSection)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width - 35) / 2 , height:  collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let paddingInset: CGFloat = 10.0
        return UIEdgeInsets(top: 0, left: paddingInset, bottom: 0, right: paddingInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 5.0
     }
}
