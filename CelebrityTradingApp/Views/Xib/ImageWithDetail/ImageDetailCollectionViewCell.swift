//
//  ImageDetailCollectionViewCell.swift
//  CelebrityTradingApp
//
//  Created by Vishwesh on 07/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class ImageDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var changeInPriceStack: UIStackView!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var boxButton: UIButton!
    
    enum Playlist: Int {
        case RecentlyPlayed = 0
        case Playlists
        case PeopleBought
        case TeenPop
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.boxButton.isHidden = true
    }
    
    func populateData(section: Int) {
        if section == 0 {
            self.boxButton.isHidden = false
        } else {
            self.boxButton.isHidden = true
        }
    }
    
}
