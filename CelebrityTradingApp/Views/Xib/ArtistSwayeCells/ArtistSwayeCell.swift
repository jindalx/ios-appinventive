//
//  ArtistSwayeCell.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 21/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class ArtistSwayeCell: UITableViewCell {

    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var swayePriceLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var valueStatusImage: UIImageView!
    
    private let randomImages: [UIImage] = [#imageLiteral(resourceName: "icArrowupgreen"), #imageLiteral(resourceName: "iconDownArrow")]

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureData(_ artist: Artist) {
        self.artistName.text = artist.name
        self.artistImage.setImage(withUrl: artist.img, placeholderImage: UIImage(), showIndicator: true)
        self.swayePriceLbl.text = "$\(artist.swayePrice)"
        self.valueLbl.text = "\(artist.changeInPrice)%"
         staticData(row: 0)
    }

    func configurePortofolioData(_ userPortfolio: UserPortofolio) {
        self.artistName.text = userPortfolio.artist.name
        self.artistImage.setImage(withUrl: userPortfolio.artist.img, placeholderImage: UIImage(), showIndicator: true)
        self.swayePriceLbl.text = "$\(userPortfolio.swayePrice)"
        self.valueLbl.text = "$\(userPortfolio.changeInPrice)"
        staticData(row: 0)
    }
    
    func staticData(row: Int) {
        self.valueStatusImage.image = row%2 == 0 ? self.randomImages[0] : self.randomImages[1]
        self.valueLbl.textColor = row%2 == 0 ? AppColors.appDarkGreen : AppColors.appRedColor
    }
    
   private func randomStatusImage() -> UIImage {
        let unsignedArrayCount = UInt32(randomImages.count)
        let unsignedRandomNumber = arc4random_uniform(unsignedArrayCount)
        let randomNumber = Int(unsignedRandomNumber)
        return randomImages[randomNumber]
    }
    
}

