//
//  CashRecordCell.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 04/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class CashRecordCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
