//
//  MusicTabBarView.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit
import GradientProgressBar

class MusicTabBarView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var audioTitle: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var progressBarView: GradientProgressBar!
    @IBOutlet weak var buyAndTradeBtn: UIButton!
    @IBOutlet weak var buyButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var buyButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var playPauseButtonTrailing: NSLayoutConstraint!
    
    private var shouldHideBuyButton: Bool = false {
        didSet {
            self.buyButtonWidth.constant = shouldHideBuyButton ? 0 : 85.0/375.0
            self.playPauseButtonTrailing.constant = shouldHideBuyButton ? 15.0 : 120.0
            self.buyButtonTrailingConstraint.constant = shouldHideBuyButton ? 0 : 15.0
            self.buyAndTradeBtn.isHidden = shouldHideBuyButton
            self.layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        confirmBtnSetUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        Bundle.main.loadNibNamed("MusicTabBarView", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [
            .flexibleWidth,
            .flexibleHeight
        ]
        
        self.albumImage.cornerRadius = 6
        
        self.musicBarSetup()
     
         NotificationCenter.default.addObserver(self, selector: #selector(self.changeMusicButton(notification:)), name: NSNotification.Name(rawValue: "changeMusicButton"), object: nil)
        self.contentView.backgroundColor = .clear
        self.backgroundImage.image =  #imageLiteral(resourceName: "clear")
        //self.shouldHideBuyButton = true
    }
    
    @objc func changeMusicButton(notification : NSNotification) {
           if let dict = notification.userInfo as NSDictionary? {
            if let buttonType = dict["musicButtonType"] as? MusicButtonType, let buyButtonHide = dict["isHide"] as? Bool {
                let buttonTitle = buttonType == .HOME ? "BUY" : "TRADE"
                self.buyAndTradeBtn.setTitle(buttonTitle, for: .normal)
                self.shouldHideBuyButton = buyButtonHide
            }
        }
    }
    
    private func confirmBtnSetUp() {
        self.buyAndTradeBtn.layer.masksToBounds = true
        self.buyAndTradeBtn.applyGradient(colors: [UIColor.init(r: 34, g: 131, b: 184, alpha: 1.0).cgColor,UIColor.init(r: 15, g: 76, b: 130, alpha: 1.0).cgColor])
        //self.shouldHideBuyButton = true
    }
    
    private func musicBarSetup() {
        self.progressBarView.gradientColors = [
            .colorRGB(r: 33, g: 100, b: 171),
            .colorRGB(r: 0, g: 127, b: 142),
            .colorRGB(r: 255, g: 255, b: 255)
        ]
        
        self.progressBarView.progress = 0.0
        self.progressBarView.animationDuration = 2
        
        delay(1) {
            self.progressBarView.setProgress(1, animated: true)
        }
        self.buyAndTradeBtn.roundCornersWithBorder(weight: 0.0,borderColor: UIColor.clear)
    }
    
    @IBAction func onTapBuyAndTradeBtn(_ sender: UIButton) {
        
        // TODO - Common
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BuyArtistVC") as! BuyArtistVC
        vc.currentVC = musicButtonType == .HOME ? .buy : .sell
        let nav = UINavigationController(rootViewController: vc)
        
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = true
        
        Router.shared.currentTabNavigation?.present(nav, animated: true, completion: nil)
    }
}
