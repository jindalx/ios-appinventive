//
//  ArtistHeaderCell.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 21/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class ArtistHeaderCell: UITableViewHeaderFooterView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var viewAll: UIButton!
    @IBOutlet weak var artistHeaderLbl: UILabel!
    @IBOutlet weak var priceHeaderLbl: UILabel!
    @IBOutlet weak var valueHeaderLbl: UILabel!
    @IBOutlet weak var bottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var calenderStackView: UIStackView!
    @IBOutlet var calendarSlotButtons: [CustomButton]!
    
    var btnViewAllAction : (()->Void)?
    
    var titleNameArr = [LocalString.myCashTitle,LocalString.mySwayeTitle, LocalString.myWatchlistTitle]
    var titleName_WatchListArr = [LocalString.myCashTitle, LocalString.myWatchlistTitle]
    
    let titleFontArr = [UIFont(name: LocalString.TextRegTitle, size: 15), UIFont(name: LocalString.displayBoldTitle, size: 25), UIFont(name: LocalString.displaySemiboldTitle, size: 19)]
    
    var userBalance: UserBalance?
    
//    override open var isHighlighted: Bool {
//        didSet {
//            backgroundColor = isHighlighted ? UIColor.black : UIColor.white
//        }
//    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewAll.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        viewAll.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        viewAll.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
    }
    
    func populateData(section : Int, screenType: HomeScreeType) {
        
        self.highlightCalendarSlot(SlotType.hour.rawValue)
        if screenType == .ForYou {
             self.viewAll.setTitle(LocalString.seeAllTitle, for: .normal)
            return
        }
        
        if screenType == .Watchlist {
            self.hideCalenderView(section: section)
            self.titleLbl.text = titleName_WatchListArr[section]
        } else {
            self.titleLbl.text = titleNameArr[section]
        }
        self.titleLbl.font = self.titleFontArr[section]
        
        if section == 0 {
            if let userBalance = self.userBalance {
                //TODO: - balance update
                self.viewAll.setTitle("$50.0000", for: .normal)
               // self.viewAll.setTitle(userBalance.balance, for: .normal)
            } else {
                self.viewAll.setTitle("$50.0000", for: .normal)
            }
            self.viewAll.setImage(#imageLiteral(resourceName: "icArrowforward-1"), for: .normal)
            self.viewAll.setTitleColor(UIColor.AppColor.changeBlack, for: .normal)
            
        } else {
            
            if screenType == .Watchlist {
                self.viewAll.isHidden = true
            } else {
                if screenType == .Home {
                    self.viewAll.setTitle(LocalString.seeAllTitle, for: .normal)
                } else if screenType == .Swaye {
                    self.viewAll.setTitle(LocalString.historyTitle, for: .normal)
                }
                
                self.viewAll.setImage(#imageLiteral(resourceName: "icArrowforward"), for: .normal)
                self.viewAll.setTitleColor(UIColor.AppColor.changeBlue, for: .normal)
            }
            
            if section == 1 {
                if screenType == .Watchlist {
                    self.valueHeaderLbl.text = LocalString.myWatchListHRPerlistTitle
                } else  {
                    self.valueHeaderLbl.text = LocalString.myValuelistTitle
                }
            } else {
                self.valueHeaderLbl.text = LocalString.myHRPercentageTitle
            }
        }
        
        self.hideHeader(section: section)
    }
    
    private func hideHeader(section : Int) {
        if section == 0 {
            
            self.artistHeaderLbl.isHidden = true
            self.priceHeaderLbl.isHidden = true
            self.valueHeaderLbl.isHidden = true
            self.bottomConstraints.constant = 10
            self.containerView.drawDropShadow(color: #colorLiteral(red: 0.8823529412, green: 0.8823529412, blue: 0.8823529412, alpha: 1))
            
        } else {
            
            self.artistHeaderLbl.isHidden = false
            self.priceHeaderLbl.isHidden = false
            self.valueHeaderLbl.isHidden = false
            self.bottomConstraints.constant = 0
            self.containerView.layer.shadowOpacity = 0
        }
    }
    
    //IBActions
    @IBAction func onTapSeeAllBtn(_ sender: UIButton) {
        self.btnViewAllAction?()
    }
    
    @IBAction func tapOn24h(_ sender: CustomButton) {
        self.highlightCalendarSlot(sender.tag)
    }
}



//Extension to handle the calnder view on Watchlist screen
extension ArtistHeaderCell {
    
    fileprivate func hideCalenderView(section : Int) {
        if section == 1 {
            self.calenderStackView.isHidden = false
        } else {
            self.calenderStackView.isHidden = true
        }
    }
    
    fileprivate func highlightCalendarSlot(_ slotType: Int) {
        for slotButton in self.calendarSlotButtons {
            slotButton.isSelected = false
            if slotType == slotButton.tag {
                slotButton.isSelected = true
            }
        }
    }
}
