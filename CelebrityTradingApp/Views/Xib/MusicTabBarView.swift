//
//  MusicTabBarView.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class MusicTabBarView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var audioTitle: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        Bundle.main.loadNibNamed("MusicTabBarView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [
            .flexibleWidth,
            .flexibleHeight
        ]
        
        self.albumImage.cornerRadius = 6
    }
}
