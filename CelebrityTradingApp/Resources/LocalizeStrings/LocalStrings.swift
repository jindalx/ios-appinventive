//
//  LocalStrings.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation

enum LocalString {
    //MARK:- TabBar Titles
    static var homeTitle    : String { return "home".localized }
    static var forYouTitle  : String { return "forYou".localized }
    static var artistTitle  : String { return "artist".localized }
    static var alertTitle   : String { return "alert".localized }
    static var accountTitle : String { return "account".localized }
    static var ok : String { return "ok".localized }
    static var pleaseCheckInternetConnection: String {return "Please Check Internet Connection".localized }
    
    static var myCashTitle      : String { return "myCash:".localized }
    static var mySwayeTitle     : String { return "mySwaye".localized }
    static var myWatchlistTitle : String { return "myWatchlist".localized }
    static var myValuelistTitle : String { return "myValue".localized }
    static var myWatchListHRPerlistTitle : String { return "myWatchListHRPerValue".localized }
    static var pendingTitle : String { return "pending".localized }
    static var confirmTitle : String { return "confirm".localized }
    
    static var summaryTitle    : String { return "summary".localized }
    static var transfersTitle  : String { return "transfers".localized }
    static var statementsTitle : String { return "statements".localized }
    static var settingsTitle   : String { return "settings".localized }
    static var helpTitle       : String { return "help".localized }

    static var summaryDescTitle    : String { return "summaryDesc".localized }
    static var transfersDescTitle  : String { return "transfersDesc".localized }
    static var historyDescTitle    : String { return "historyDesc".localized }
    static var statementsDescTitle : String { return "statementsDesc".localized }
    static var settingsDescTitle   : String { return "settingsDesc".localized }
    static var helpDescTitle       : String { return "helpDesc".localized }
    
    
    static var myHRPercentageTitle  : String { return "myHRPercentage".localized }
    static var seeAllTitle      : String { return "seeAll".localized }
    static var historyTitle     : String { return "history".localized }
    static var TextRegTitle     : String { return "SFProText-Regular".localized }
    static var displayBoldTitle : String { return "SFProDisplay-Bold".localized }
    static var displaySemiboldTitle : String { return "SFProDisplay-Semibold".localized }
    
    //Fpr you
    static var recentlyPlayedTitle : String {return "recentlyPlay".localized}
    static var playlistTitle : String {return "playlist".localized}
    static var peopleBoughtTitle : String {return "peopleBought".localized}
    static var teenTopTitle : String {return "teenTop".localized}
}

