//
//  BuyArtistPendingVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class BuyArtistPendingVC: UIViewController {
    
    //MARK:- PROPERTIES
    //================
    var currentVC : TypeOfVC  = .buy
    var artistHeaderView = ArtistHeaderView.instanciateFromNib()
    
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet var pendingFooterView: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cancelBtnSetUp()
    }
    
    //MARK:- PRIVATE FUNCTIONS
    //========================
    
    private func initialSetUp(){
        self.title = LocalString.pendingTitle
        self.tableViewSetUp()
        self.registerXib()
        self.footerSetUp()
        self.headerViewSetUp()
    }
    
    
    
    
    private func tableViewSetUp() {
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        
    }
    
    private func registerXib() {
        self.mainTableView.registerCell(with: PendingArtistCell.self)
        self.mainTableView.registerCell(with: PendingHistoryCell.self)
        self.mainTableView.registerHeaderFooter(with: PendingArtistHeaderCell.self)
    }
    
    private func headerViewSetUp() {
        self.headerView.height = (screen_height * 375) / 812
        self.headerView.width = screen_width
        self.artistHeaderView.frame = CGRect.init(x: 0, y: 0, width: self.headerView.width, height: self.headerView.height)
        self.artistHeaderView.currentlyUsingAs = .buyPending
        self.headerView.addSubview(artistHeaderView)
    }
    
    private func cancelBtnSetUp() {
        self.doneBtn.layer.masksToBounds = true
        self.doneBtn.layer.cornerRadius = self.doneBtn.height / 2.0
        self.doneBtn.applyGradient(colors: [UIColor.init(r: 34, g: 131, b: 184, alpha: 1.0).cgColor,UIColor.init(r: 15, g: 76, b: 130, alpha: 1.0).cgColor])
    }
    
    private func footerSetUp(){
        self.cancelBtn.setTitleColor(UIColor.init(r: 33, g: 100, b: 171, alpha: 1.0), for: .normal)
        self.mainTableView.tableFooterView = pendingFooterView
        self.mainTableView.tableFooterView?.height = 130.0
    }
    
    
    
    //MARK:- IBACTIONS
    //================
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- TABLEVIEW DELEGATE AND DATASOURCE
//================

extension BuyArtistPendingVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueCell(with: PendingArtistCell.self)
            cell.swayePriceLbl.text = self.currentVC == .buy ? "Quantity" : "Swaye Price"
            cell.cashLeadingConst.isActive = self.currentVC == .sell ? false : true
            cell.filledSwayeTitleLbl.text = self.currentVC == .sell ? "Requested Swaye Price" : "Filled Swaye"
            return cell
        default:
            let cell = tableView.dequeueCell(with: PendingHistoryCell.self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
              return (250 * screen_height) / 812
        } else { return 65.0 }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { return 0 }
        else { return 103.0 }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PendingArtistHeaderCell")
            return view
        }
        else { return nil }
    }
    
}


extension BuyArtistPendingVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
               (self.navigationController?.navigationBar.frame.height ?? 0.0)
        if scrollView.contentOffset.y >= topBarHeight - 20.0 {
             self.title = ""
        } else {
            self.title = LocalString.pendingTitle
        }
    }
}
