//
//  PendingArtistCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class PendingArtistCell: UITableViewCell {
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var submittedDateLbl: UILabel!
    @IBOutlet weak var swayePriceLbl: UILabel!
    @IBOutlet weak var filledSwayeLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var swayePriceTitleLbl: UILabel!
    @IBOutlet weak var filledSwayeTitleLbl: UILabel!
    @IBOutlet weak var cashTitleLbl: UILabel!
    @IBOutlet weak var cashLeadingConst: NSLayoutConstraint!
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
