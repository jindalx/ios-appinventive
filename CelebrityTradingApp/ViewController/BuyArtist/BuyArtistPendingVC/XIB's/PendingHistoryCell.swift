//
//  PendingHistoryCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class PendingHistoryCell: UITableViewCell {
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var submittedDate: UILabel!
    @IBOutlet weak var swayeLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var submittedTimeLbl: UILabel!
    
    
    
    
    //MARK:- VIEW LIFE CYCLE
    //================
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
