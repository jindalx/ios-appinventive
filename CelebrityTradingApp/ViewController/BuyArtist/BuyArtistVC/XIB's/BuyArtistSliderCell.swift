//
//  BuyArtistSliderCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 25/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

protocol BuyArtistSliderCellDelegate : class {
    func sliderValueChanged(indexPath: IndexPath,sender: CustomSlider)
}

class BuyArtistSliderCell: UITableViewCell {
    
    var lblSliderValue = UILabel()
    weak var delegate:BuyArtistSliderCellDelegate?

    @IBOutlet weak var slider: CustomSlider!
    @IBOutlet weak var sliderLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.slidingLabelSetUp()
        self.slider.minimumValue = 1.0
        self.slider.maximumValue = 60.0
        self.slider.value = 5.0
        delay(0.1) {
            self.delegate?.sliderValueChanged(indexPath: IndexPath(item: 0, section: 0), sender: self.slider)
            self.delegate?.sliderValueChanged(indexPath: IndexPath(item: 1, section: 0), sender: self.slider)
            self.delegate?.sliderValueChanged(indexPath: IndexPath(item: 2, section: 0), sender: self.slider)
        }
    }
    
    private func slidingLabelSetUp() {
           self.lblSliderValue.textAlignment = .center
           self.lblSliderValue.textColor = .white
           self.lblSliderValue.backgroundColor = UIColor.init(r: 20, g: 93, b: 143, alpha: 1.0)
           self.slider.addSubview(lblSliderValue)
       }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.lblSliderValue.layer.masksToBounds = true
    }
    
    
    @IBAction func sliderValueChanged(_ sender: CustomSlider) {
        if let superVw = self.superview as? UITableView, let indexPath = superVw.indexPath(for: self) {
            self.delegate?.sliderValueChanged(indexPath: indexPath, sender: sender)
        }
        
    }
}
