//
//  ArtistHeaderView.swift
//  CelebrityTradingApp
//
//  Created by Admin on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//
import UIKit
import Foundation

class ArtistHeaderView: UIView {
    
    enum typeOfView {
        case buyArtist
        case buyConfirm
        case buyPending
    }
    
    //MARK:- IBOUTLETS
    //==================
    
    
    @IBOutlet weak var imgContainerView: UIView!
    @IBOutlet weak var headerImgBtmConst: NSLayoutConstraint!
    @IBOutlet weak var buyArtistView: UIView!
    @IBOutlet weak var confirmArtistView: UIView!
    @IBOutlet weak var headerbackgroundView: UIView!
    @IBOutlet weak var headerImgView: UIImageView!
    @IBOutlet weak var availCashLbl: UILabel!
    @IBOutlet weak var feeLbl: UILabel!
    @IBOutlet weak var confirmArtistSubTitleLbl: UILabel!
    @IBOutlet weak var buyArtistStackView: UIStackView!
    
    
    var currentlyUsingAs = typeOfView.buyArtist {
           didSet {
               switch currentlyUsingAs {
               case .buyArtist:
                   self.setupForBuyArtist()
                   
               case .buyConfirm:
                   self.setupForBuyArtistConfirm()
                   
               default:
                   self.setupForBuyArtistPending()
               }
           }
       }
    
    override func setNeedsLayout() {
        super.setNeedsLayout()
    }
    
    //MARK:- VIEW LIFE CYCLE
    //=====================
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetUp()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.headerbackgroundView.addSlope
        self.headerImgView.roundCornersWithBorder(weight: 6.0,borderColor: UIColor.AppColor.profileBorderColor)
        self.imgContainerView.roundCornersWithBorder(weight: 0.0,borderColor: UIColor.clear)
        self.imgContainerView.drawDropShadow(color: UIColor.init(r: 0, g: 0, b: 0, alpha: 0.5),is3DView : true)
        self.buyArtistView.addShadowToView(location: .top, color: UIColor.black.withAlphaComponent(0.25), opacity: 1, radius: 3)
    
    }


    
    class func instanciateFromNib() -> ArtistHeaderView {
        return Bundle .main .loadNibNamed("ArtistHeaderView", owner: self, options: nil)![0] as! ArtistHeaderView
    }
    
    
    
    //MARK:- PRIVATE FUNCTIONS
    //=======================
    
    private func initialSetUp() {
//        self.headerImgSetUp()
        
    }
    
    private func headerImgSetUp() {
        self.headerbackgroundView.layer.masksToBounds = true
        self.headerbackgroundView.addGradient(colors: UIColor.AppColor.artistHeaderGradientColors)
        self.imgContainerView.drawDropShadow(color: .black)
        
    }
    
    private func setupForBuyArtist() {
        self.headerImgBtmConst.isActive = true
        self.confirmArtistView.isHidden = true
        self.layoutIfNeeded()
        self.headerImgSetUp()
    }
    
    private func setupForBuyArtistConfirm() {
        self.headerImgBtmConst.isActive = false
        self.buyArtistView.isHidden = true
        self.layoutIfNeeded()
        self.headerImgSetUp()
    }
    
    private func setupForBuyArtistPending() {
       
        self.headerImgBtmConst.isActive = false
        self.buyArtistView.isHidden = true
        self.confirmArtistSubTitleLbl.isHidden = true
        self.layoutIfNeeded()
        self.headerImgSetUp()
    }
    
    
}
