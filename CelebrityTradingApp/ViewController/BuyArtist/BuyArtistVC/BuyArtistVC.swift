//
//  BuyArtistVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

enum TypeOfVC {
    case buy
    case sell
}

import UIKit

class BuyArtistVC: UIViewController {
    
   
    //MARK:- PROPERTIES
    //================
    
    typealias RequiredParams = ()
    var currentVC: TypeOfVC = .buy
    var artistHeaderView =  ArtistHeaderView.instanciateFromNib()
    var model = BuyArtistModel()
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIImageView!
    @IBOutlet weak var confirmBtn: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    //=====================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.confirmBtnSetUp()
    }
    
    
    //MARK:- PRIVATE FUNCTIONS
    //=====================
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.regsiterXib()
        self.headerViewSetUp()
        self.footerSetUp()
        self.setNavigationBarColor()
        self.addButtonOnLeft()
        self.model.fee = "0.9900"
        
        
    }
    
    private func setNavigationBarColor(){
        
        self.title = self.currentVC == .buy ? "Buy Billie Eilish" : "Sell Billie Eilish"
        let navBar = self.navigationController?.navigationBar
        navBar?.tintColor = UIColor.white
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navBar?.titleTextAttributes = textAttributes
        self.navigationItem.backBarButtonItem = UIBarButtonItem(
               title: "", style: .plain, target: nil, action: nil)
    }
    
    private func tableViewSetUp() {
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
    }
    
    private func regsiterXib(){
        self.mainTableView.registerCell(with: BuyArtistSliderCell.self)
    }
   
    
    private func confirmBtnSetUp() {
        self.confirmBtn.applyGradient(colors: [UIColor.init(r: 34, g: 131, b: 184, alpha: 1.0).cgColor,UIColor.init(r: 15, g: 76, b: 130, alpha: 1.0).cgColor])
        self.confirmBtn.layer.masksToBounds = true
        self.confirmBtn.layer.cornerRadius =   self.confirmBtn.height / 2
    }
    
    private func footerSetUp(){
        self.mainTableView.tableFooterView = footerView
        self.mainTableView.tableFooterView?.height = 130.0
        self.mainTableView.tableFooterView?.width = screen_width
    }
    
    
    private func headerViewSetUp() {
        self.headerView.height = (screen_height * 315) / 812
        self.headerView.width = screen_width
        self.artistHeaderView.frame = CGRect.init(x: 0, y: 0, width: self.headerView.width, height: self.headerView.height)
        self.artistHeaderView.currentlyUsingAs = .buyArtist
        self.artistHeaderView.buyArtistStackView.isHidden = (self.currentVC == .buy) ? false : true
        self.headerView.addSubview(artistHeaderView)
    }
    
    func setUISliderThumbValueWithLabel(slider: UISlider) -> CGPoint {
        let slidertTrack : CGRect = slider.trackRect(forBounds: slider.bounds)
        let sliderFrm : CGRect = slider.thumbRect(forBounds: slider.bounds, trackRect: slidertTrack, value: slider.value)
        return CGPoint(x: sliderFrm.origin.x + slider.frame.origin.x - 10.0, y: -30.0 )
    }
    
    
    //MARK:- IBACTIONS
    //=====================
    
    
    @IBAction func confirmBtnAction(_ sender: UIButton) {
        let vc = BuyArtistConfirmVC.instantiate(fromAppStoryboard: .Home)
        vc.currentVC = self.currentVC
        vc.model = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}


//MARK:- CustomSlider
//================

class CustomSlider: UISlider {
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 10.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
    
}

//MARK:- Table View data Source
//===========================
extension BuyArtistVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: BuyArtistSliderCell.self)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
    
}
//MARK:- BuyArtistSliderCellDelegate
//===========================

extension BuyArtistVC: BuyArtistSliderCellDelegate {
    func sliderValueChanged(indexPath: IndexPath,sender: CustomSlider) {
        switch indexPath.row {
        case 0:
            guard let cell = self.mainTableView.cellForRow(at: indexPath) as? BuyArtistSliderCell else { fatalError("cell nt found")}
            let title = self.currentVC == .buy ? "Number of SWAYE: MAX " : "SWAYE: "
            cell.sliderLbl.text = "\(title)" + "\(sender.value)"
            cell.lblSliderValue.text = "\(sender.value)"
            let currentWidth = cell.lblSliderValue.text?.sizeCount(withFont: UIFont.italicSystemFont(ofSize: 15), boundingSize: CGSize(width: 10000.0, height: 30.0)).width ?? 0.0
            cell.lblSliderValue.frame = CGRect.init(x: 0.0, y: 0.0, width: (currentWidth + 25.0), height: 30.0)
            self.model.noOfSwaye = "\(sender.value)"
            cell.lblSliderValue.layer.cornerRadius = 15.0
            cell.lblSliderValue.center = setUISliderThumbValueWithLabel(slider: sender)
        case 1:

           guard let cell = self.mainTableView.cellForRow(at: indexPath) as? BuyArtistSliderCell else { fatalError("cell nt found")}
            cell.lblSliderValue.text = "$" + "\(sender.value)"
            let title = self.currentVC == .buy ? "SWAYE VALUE: LAST " : "SWAYE PRICE: "
            cell.sliderLbl.text = "\(title)" + "$" + "\(sender.value)"
            let currentWidth = cell.lblSliderValue.text?.sizeCount(withFont: UIFont.italicSystemFont(ofSize: 15), boundingSize: CGSize(width: 10000.0, height: 30.0)).width ?? 0.0
           cell.lblSliderValue.frame = CGRect.init(x: 0.0, y: 0.0, width: (currentWidth + 30.0), height: 30.0)
            self.model.swayePrice =  "\(sender.value)"
            cell.lblSliderValue.layer.cornerRadius = 15.0
            cell.lblSliderValue.center = setUISliderThumbValueWithLabel(slider: sender)
        default:
            guard let cell = self.mainTableView.cellForRow(at: indexPath) as? BuyArtistSliderCell else { fatalError("cell nt found")}
            cell.lblSliderValue.text = "$" + "\(sender.value)"
            let title = self.currentVC == .buy ? "SWAYE VALUE: MAX $" + "\(sender.value)" : "CASH"
            cell.sliderLbl.text = "\(title)"
            let currentWidth = cell.lblSliderValue.text?.sizeCount(withFont: UIFont.italicSystemFont(ofSize: 15), boundingSize: CGSize(width: 10000.0, height: 30.0)).width ?? 0.0
            cell.lblSliderValue.frame = CGRect.init(x: 0.0, y: 0.0, width: (currentWidth + 30.0), height: 30.0)
            self.model.swayeValue = "\(sender.value)"
            cell.lblSliderValue.layer.cornerRadius = 15.0
            cell.lblSliderValue.center = setUISliderThumbValueWithLabel(slider: sender)
           
            
        }
    }
}



//MARK:- UINavigationItem
//================

extension BuyArtistVC {
    
   
   func addButtonOnLeft(){
    let barButtonItem = UIBarButtonItem(image: UIImage(named: "icClose"),
    style: .plain,
    target: self,
    action: #selector(gotBackPage))
    self.navigationItem.leftBarButtonItem = barButtonItem
  }

    @objc func gotBackPage() {
        self.dismiss(animated: true, completion: nil)
  }
}
