//
//  BuyArtistConfirmCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 26/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class BuyArtistConfirmCell: UITableViewCell {
    
    @IBOutlet weak var noOfSwayeLbl: UILabel!
    @IBOutlet weak var swayePriceLbl: UILabel!
    @IBOutlet weak var swayeValueLbl: UILabel!
    @IBOutlet weak var feeLbl: UILabel!
    @IBOutlet weak var totalFeeLbl: UILabel!
    @IBOutlet weak var swayeValueTitleLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    
    func populateCell(model: BuyArtistModel) {
        self.noOfSwayeLbl.text = model.noOfSwaye
        self.swayePriceLbl.text = "$" + model.swayePrice
        self.swayeValueLbl.text = "$" + model.swayeValue
        self.feeLbl.text = "$" + model.fee
        self.totalFeeLbl.text = "$" + String((model.swayeValue as NSString).floatValue + (model.fee as NSString).floatValue)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
}
