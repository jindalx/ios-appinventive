//
//  BuyArtistConfirmVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class BuyArtistConfirmVC: UIViewController,VCConfigurator {
    
 
    //MARK:- PROPERTIES
    //================
    
    var currentVC: TypeOfVC = .buy
    typealias RequiredParams = (BuyArtistModel)
    var model : BuyArtistModel?
    var artistHeaderView =  ArtistHeaderView.instanciateFromNib()
    
    //MARK:- IBOUTLETS
    //================
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet var footerView: UIView!
    
    //MARK:- VIEW LIFE CYCLE
    //================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    
    //MARK:- PRIVATE FUNCTIONS
    //================
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.submitBtnSetUp()
        
    }
    
    
    func configureWithParameters(param: (BuyArtistModel)) {
        self.model  = param
     }
     
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.headerViewSetUp()
        self.regsiterXib()
        self.footerSetUp()
        self.title = LocalString.confirmTitle
        self.navigationItem.backBarButtonItem = UIBarButtonItem(
        title: "", style: .plain, target: nil, action: nil)
        self.addButtonOnRight()
        
    }
    
    
    private func tableViewSetUp(){
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
    }
    
    private func regsiterXib(){
        self.mainTableView.registerCell(with: BuyArtistConfirmCell.self)
    }
    
  
    
    private func submitBtnSetUp() {
        self.submitBtn.applyGradient(colors: [UIColor.init(r: 34, g: 131, b: 184, alpha: 1.0).cgColor,UIColor.init(r: 15, g: 76, b: 130, alpha: 1.0).cgColor])
        self.submitBtn.layer.masksToBounds = true
        self.submitBtn.layer.cornerRadius = self.submitBtn.height / 2.0
    }
    
    private func footerSetUp(){
        self.mainTableView.tableFooterView = footerView
        self.mainTableView.tableFooterView?.height = 130.0
        self.mainTableView.tableFooterView?.width = screen_width
    }
    
    
    private func headerViewSetUp() {
        self.headerView.height = (screen_height * 375) / 812
        self.headerView.width = screen_width
        self.artistHeaderView.frame = CGRect.init(x: 0, y: 0, width: self.headerView.width, height: self.headerView.height)
        self.artistHeaderView.currentlyUsingAs = .buyConfirm
        self.headerView.addSubview(artistHeaderView)
    }
    
    
   
    
    //MARK:- IBACTIONS
    //================
    @IBAction func submitBtnAction(_ sender: UIButton) {

        let vc = BuyArtistPendingVC.instantiate(fromAppStoryboard: .Home)
        vc.currentVC = self.currentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK:- Table View Data Source and delegate
  //================

extension BuyArtistConfirmVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: BuyArtistConfirmCell.self)
        cell.populateCell(model: model ?? BuyArtistModel())
        cell.swayeValueTitleLbl.text = self.currentVC == .buy ?   "Swaye Value" : "Cash"
        cell.totalTitleLbl.text = self.currentVC == .buy ?   "Total" : "Cash:"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (250 * screen_height) / 812
    }
    
}


//MARK:- UINavigationItem
//================

extension BuyArtistConfirmVC {
    
   
   func addButtonOnRight(){
    let button = UIButton(type: .custom)
    button.setTitle("Cancel", for: .normal)
    button.titleLabel?.font = AppFonts.sfProDisplayMedium.withSize(15.0)
    button.backgroundColor = UIColor.clear
    button.addTarget(self, action: #selector(gotBackPage), for: UIControl.Event.touchUpInside)
    let barButton = UIBarButtonItem(customView: button)
    self.navigationItem.rightBarButtonItem = barButton
  }

    @objc func gotBackPage() {
        self.dismiss(animated: true, completion: nil)
  }
}
