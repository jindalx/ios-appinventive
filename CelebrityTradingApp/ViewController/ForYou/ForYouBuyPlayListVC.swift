//
//  ForYouViewController.swift
//  CelebrityTradingApp
//
//  Created by Vishwesh on 07/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class ForYouBuyPlayListVC: BaseVC {

    internal let headerData: [String] = [LocalString.recentlyPlayedTitle, LocalString.playlistTitle, LocalString.peopleBoughtTitle, LocalString.teenTopTitle]
    
    @IBOutlet weak var buyPlaylistTableView: UITableView!
    
    override func registerNibs() {
        super.registerNibs()
        self.buyPlaylistTableView.registerCell(with: ImageDetailTableViewCell.self)
        self.buyPlaylistTableView.registerHeaderFooter(with: ForYouHeaderFooterView.self)
        self.buyPlaylistTableView.delegate = self
        self.buyPlaylistTableView.dataSource = self
    }
    
    override func initialSetup() {
        super.initialSetup()
       // let title = "Monday, February 3 \nForYou"
        self.setNavigationBar(title: "ForYou", backButton: false, titleView: false, largeTitles: true)
        self.addRightButtonToNavigation(image: UIImage(named: "icSearch"))
    }
    
}
