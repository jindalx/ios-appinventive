//
//  ForYouVC+ViewCells.swift
//  CelebrityTradingApp
//
//  Created by Vishwesh on 07/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

//MARK:- TableViewCells
extension ForYouBuyPlayListVC {
    
    /// Get Title Cell
    internal func getForYouImageDetailCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: ImageDetailTableViewCell.self, indexPath: indexPath)
        cell.currentSection = indexPath.section
        cell.clipsToBounds = true
        return cell
    }
    
    internal func getArtistHeaderCell(_ tableview: UITableView, section: Int) -> UIView {
        guard let headerView = tableview.dequeueReusableHeaderFooterView(withIdentifier: "ForYouHeaderFooterView") as? ForYouHeaderFooterView else {
            return UIView()
        }
        headerView.populateData(title: self.headerData[section])
        return headerView
    }
    
}
