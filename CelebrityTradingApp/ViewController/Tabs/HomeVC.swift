//
//  HomeVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeVC: BaseVC {

    private let controller = ArtistController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.controller.getAllArtists { (news, error) -> (Void) in
            if let error = error {
                self.showAlert(msg: error.localizedDescription)
            }
        }
    }
    
}
