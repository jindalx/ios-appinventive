//
//  DayWiseCell.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 21/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class DayWiseCell: UICollectionViewCell {
    
    @IBOutlet weak var timeRangeLbl: UILabel!
    
    let data = ["24h", "1w", "1m", "3m","6m","1y"]

    
    override var isSelected: Bool{
        didSet {
            if self.isSelected {
                self.contentView.backgroundColor = UIColor.AppColor.changeLightTeal
            } else {
                self.contentView.backgroundColor = UIColor.clear
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
    }
    
    func populateDate(item : Int) {
        self.timeRangeLbl.text = self.data[item]
    }
    
    func settingPopulateData() {
        
    }
}
