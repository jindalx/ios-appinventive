//
//  HomeVC+Delegates.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 21/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //Change it with data
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getDayShareStatusCell(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (screen_width*40)/375, height: (screen_height*40)/812)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return (screen_width*15)/375
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let top     = (screen_height*2)/812
        let left    = (screen_width*14)/375
        let bottom  = (screen_height*15)/812
        let right   = (screen_width*14)/375
        
        return UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        self.generateRandomDataToChart()
        
        if cell?.isSelected == true {
            cell?.isSelected = false
        } else {
            cell?.isSelected = true
        }
    }
}

extension HomeVC : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        //Change it with model data
        var numberOfSec = 1
        if !userPortfolio.isEmpty {
            numberOfSec = numberOfSec + 1
        }
        if !userWatchlists.isEmpty {
           numberOfSec = numberOfSec + 1
        }
        return numberOfSec
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {return 60}
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Change it with model data
        if section == 0 {return 0}
        else if section == 1 {return self.isAPICalled ? self.userPortfolio.count == 0 ? 5 : self.userPortfolio.count : 0 }
        else {return self.isAPICalled ? self.userWatchlists.count == 0 ? 5 : self.userWatchlists.count : 0}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getArtistShareCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getArtistHeaderCell(tableView, section: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc  = SwayeOtherUserVC.instantiate(fromAppStoryboard: .Home)
        
        if indexPath.section == 1 {
            vc.userType = .swaye
            vc.userPortFolio = self.userPortfolio[indexPath.row]
        } else if indexPath.section == 2 {
             vc.userType = .watchList
             vc.artistDetail = self.userWatchlists[indexPath.row]
        }
        self.navigationItem.title = "Artists"
        Router.shared.currentTabNavigation?.pushViewController(vc, animated: true)
    }
}
