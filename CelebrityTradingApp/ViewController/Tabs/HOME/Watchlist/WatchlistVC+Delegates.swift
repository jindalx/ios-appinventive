//
//  WatchlistVC+Delegates.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

extension WatchlistVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //Change it with model data
        return self.userWatchlists.isEmpty ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {return 60}
        return 100 + 30 + (screen_height*40)/812
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Change it with model data
        if section == 0 {return 0}
        return self.isAPICalled ? self.userWatchlists.count == 0 ? 5 : self.userWatchlists.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getArtistShareCell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getArtistHeaderCell(tableView, section: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc  = SwayeOtherUserVC.instantiate(fromAppStoryboard: .Home)
        
        if indexPath.section == 1 {
            vc.userType = .watchList
            vc.artistDetail = self.userWatchlists[indexPath.row]
        }
        
        Router.shared.currentTabNavigation?.pushViewController(vc, animated: true)
    }
}
