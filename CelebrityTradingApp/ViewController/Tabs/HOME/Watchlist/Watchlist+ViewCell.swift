//
//  Watchlist+ViewCell.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

extension WatchlistVC {
    
    internal func getArtistShareCell(_ tableview: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableview.dequeueCell(with: ArtistSwayeCell.self, indexPath: indexPath)
            return cell
        default:
            let cell = tableview.dequeueCell(with: ArtistSwayeCell.self, indexPath: indexPath)
            if userWatchlists.isEmpty {
                cell.staticData(row: indexPath.row)
            } else {
                cell.configureData(self.userWatchlists[indexPath.row])
            }
            return cell
        }
    }
    
    internal func getArtistHeaderCell(_ tableview: UITableView, section: Int) -> UIView {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ArtistHeaderCell") as? ArtistHeaderCell else {
            return UIView()
        }
        headerView.populateData(section: section, screenType: .Watchlist)
        return headerView
    }
}
