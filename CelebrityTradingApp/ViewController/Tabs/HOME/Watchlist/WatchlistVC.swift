//
//  WatchlistVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class WatchlistVC: BaseVC, VCConfigurator {

    //MARK: IBOutlets and Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerImage: UIImageView!

    typealias RequiredParams = ()
    var userWatchlists = [Artist]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    internal var isAPICalled: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUserWatchlists()
        CTASocketManager.sharedInstance.subscribeToMakret()
    }
    
     override func initialSetup(){
        super.initialSetup()
        self.headerView.height = (screen_height*284)/812
        
        self.registerNibs()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        //Adjust UITableView scroll inset
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
        self.tableView.contentInset = adjustForTabbarInsets
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
  
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMarketData), name: NSNotification.Name(rawValue: "updateMarketData"), object: nil)
       
    }
    
     override func registerNibs() {
        super.registerNibs()
        self.tableView.registerCell(with: ArtistSwayeCell.self)
        self.tableView.registerHeaderFooter(with: ArtistHeaderCell.self)
    }
    
    //VCConfigurator Delegate Methos
    func configureWithParameters(param: (String?)) {
        return
    }
    
    @objc func updateMarketData(notification : NSNotification) {
         if let dict = notification.userInfo as NSDictionary? {
             if let marketData = dict["marketData"] as? [MarketData], !marketData.isEmpty {
                 for data in marketData {
                     if let index = self.userWatchlists.firstIndex(where: {$0.symbol == data.CoinName}) {
                         self.userWatchlists[index].changeInPrice = data.ChangeInPrice
                         self.userWatchlists[index].swayePrice = data.CurrentTradingPrice
                     }
                 }
                 self.tableView.reloadData()
             }
         }
     }
}

extension WatchlistVC {
    
    func getUserWatchlists() {
        self.showSpinner()
        
        GraphQLServices.shared.getUserWatchlists(pageNo: 1, pageSize: 10, success: { (json) in
             self.isAPICalled = true
             self.removeSpinner()
             let watchlists = Artist.returnArtistsListingArray(json[ApiKey.artists])
             self.userWatchlists = watchlists
        }) { (error) -> (Void) in
            self.isAPICalled = true
            self.removeSpinner()
            CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
    }
}
