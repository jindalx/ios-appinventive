//
//  HomeVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit
import GradientProgressBar
import SwiftyJSON

class HomeVC: BaseVC {
    
    //MARK: IBOutlets and Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var shareNameValueLbl: UILabel!
    @IBOutlet weak var shareStatusContainerView: UIView!
    @IBOutlet weak var shareStatusLbl: UILabel!
    @IBOutlet weak var headerChart: LineChart!
    @IBOutlet weak var headerTimeCollection: UICollectionView!
        
    internal var userPortfolio = [UserPortofolio]()
    
    internal var userWatchlists = [Artist]()
    
    internal var userBalance: UserBalance?
    internal var isAPICalled: Bool = false
    
    private var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        musicButtonType = .HOME
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        self.headerTimeCollection.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
        self.getUserTokenDetail()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "changeMusicButton"), object: nil, userInfo: ["musicButtonType": MusicButtonType.HOME, "isHide": true])
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMarketData), name: NSNotification.Name(rawValue: "updateMarketData"), object: nil)
        
        CTASocketManager.sharedInstance.startWebSocket { (data, status, marketData) in }
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.tableView.addSubview(refreshControl)
        CTASocketManager.sharedInstance.subscribeToMakret()
        self.navigationItem.title = "Home"
        self.navigationController?.navigationBar.titleTextAttributes = [
                       NSAttributedString.Key.foregroundColor: AppColors.clear]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNavigationBarClear = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.shareStatusContainerView.roundCorners([.topRight,.bottomRight], radius: 12)
    }
    
    @objc func updateMarketData(notification : NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            if let marketData = dict["marketData"] as? [MarketData], !marketData.isEmpty {
                for data in marketData {
                    if let index = self.userWatchlists.firstIndex(where: {$0.symbol == data.CoinName}) {
                        self.userWatchlists[index].changeInPrice = data.ChangeInPrice
                        self.userWatchlists[index].swayePrice = data.CurrentTradingPrice
                    }
                    if let index = self.userPortfolio.firstIndex(where: {$0.artist.symbol == data.CoinName}) {
                        self.userPortfolio[index].changeInPrice = data.ChangeInPrice
                        self.userPortfolio[index].swayePrice = data.CurrentTradingPrice
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func refresh(sender:AnyObject)
    {
        CTASocketManager.sharedInstance.startWebSocket { (data, status, marketData) in }
        self.getUserTokenDetail()
        self.refreshControl.endRefreshing()
    }
    
    func addGradientOnHeaderImage() {
        let gradient = CAGradientLayer()
        gradient.frame = headerView.bounds
        gradient.colors = [UIColor.init(r: 86, g: 102, b: 126, alpha: 0.0).cgColor,UIColor.init(r: 14, g: 11, b: 31, alpha: 1.0).cgColor]
        headerImage.layer.addSublayer(gradient)
    }
    
    override func initialSetup() {
        super.initialSetup()

        headerTimeCollection.dataSource = self
        headerTimeCollection.delegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        
        //Adjust UITableView scroll inset
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
        self.tableView.contentInset = adjustForTabbarInsets
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
    
    }
    
    private func headerSetup() {
        delay(0.1) {
            self.addGradientOnHeaderImage()
        }
        self.headerView.height = (screen_height*284)/812
        self.headerChart.backgroundColor = .clear
        self.shareNameValueLbl.text = "Swaye Value\n$63.6600"
        self.shareStatusLbl.text = "$3.6600 (1.5%)"
        self.shareStatusContainerView.backgroundColor = UIColor.AppColor.rrowsGreen2.withAlphaComponent(0.7)
        //TO_DO
        //self.shareStatusContainerView.roundCorners([.topRight,.bottomRight], radius: 12)
        
        /*
        // Sample dataset
        let dataEntries = [PointEntry(value: 0, title: ""), PointEntry(value: 100, title: ""), PointEntry(value: 100, title: ""), PointEntry(value: 100, title: ""), PointEntry(value: 20, title: ""), PointEntry(value: 30, title: ""), PointEntry(value: 120, title: "")]
        */
        self.generateRandomDataToChart()
    }
    
    override func registerNibs() {
        super.registerNibs()
        self.tableView.registerCell(with: ArtistSwayeCell.self)
        self.tableView.registerHeaderFooter(with: ArtistHeaderCell.self)
    }
    
    
    func generateRandomDataToChart() {
        let dataEntries = generateChartEntries()
        headerChart.dataEntries = dataEntries
        headerChart.isCurved = false
    }
    
    //MARK: Populate data into chart
    private func generateChartEntries() -> [PointEntry] {
        var result: [PointEntry] = []
        for i in 0..<100 {
            let value = Int(arc4random() % 500)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM"
            var date = Date()
            date.addTimeInterval(TimeInterval(24*60*60*i))
            
            result.append(PointEntry(value: value, label: formatter.string(from: date)))
        }
        return result
    }
}

//API Delegates
extension HomeVC {
    
    func getUserTokenDetail() {
        self.showSpinner()
        var request = URLRequest(url: URL(string: "https://uoex-api.swayeformusic.com/custom/users")!)
        request.httpMethod = "GET"
       
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
            self.removeSpinner()
            if let error = error {
                CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
                DispatchQueue.mainQueueSync {
                    self.tableView.reloadData()
                }
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                    let userToken = JSON(json)[0][ApiKey.bearerToken].stringValue
                    AppUserDefaults.save(value: userToken, forKey: .authorization)
                    DispatchQueue.mainQueueSync {
                        self.getHomeData()
                    }
                } catch {
                    CommonFunctions.showToastWithMessage(msg: "Error while parsing json")
                }
            }
        })
        task.resume()
    }
    
    func getHomeData() {
        self.showSpinner()
         CTASocketManager.sharedInstance.subscribeToMakret()
        let dispatchGroup = DispatchGroup()

        dispatchGroup.enter()
        GraphQLServices.shared.getAllArtists(success: { (json) in
            
            DispatchQueue.main.async {
                let artists = UserPortofolio.returnUserPortofolioListingArray(json[ApiKey.artists])
                self.userPortfolio = artists
                self.tableView.reloadData()
                //TODO: Check leave crash solve
               //  dispatchGroup.leave()
            }
        }) { (error) -> (Void) in
            CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
      
        GraphQLServices.shared.getUserWatchlists(success: { (json) in
            
            DispatchQueue.main.async {
                let watchlists = Artist.returnArtistsListingArray(json[ApiKey.artists])
                self.userWatchlists = watchlists
                dispatchGroup.leave()
            }
            
        }) { (error) -> (Void) in
            CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
        
        GraphQLServices.shared.getUserBalance(success: { (json) in
            
            DispatchQueue.main.async {
                let userBalance = UserBalance.returnBalanceListingArray(json[ApiKey.balance])
                self.userBalance = userBalance.isEmpty ? nil : userBalance[0]
                dispatchGroup.leave()
            }
        }) { (error) -> (Void) in
            // CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
        
        dispatchGroup.notify(queue: .main) {
            self.isAPICalled = true
            self.removeSpinner()
            self.tableView.reloadData()
        }
    }
    
}
