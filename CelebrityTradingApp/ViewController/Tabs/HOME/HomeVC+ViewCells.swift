//
//  HomeVC+ViewCells.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 21/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

extension HomeVC : VCConfigurator {
    
    //MARK: Day Wise Share Status Cell
    internal func getDayShareStatusCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueCell(with: DayWiseCell.self, indexPath: indexPath)
        cell.populateDate(item : indexPath.item)
        return cell
    }
    
    internal func getArtistShareCell(_ tableview: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0,1:
            let cell = tableview.dequeueCell(with: ArtistSwayeCell.self, indexPath: indexPath)
            if userPortfolio.isEmpty {
                cell.staticData(row: indexPath.row)
            } else {
                cell.configurePortofolioData(self.userPortfolio[indexPath.row])
            }
            return cell
        default:
            let cell = tableview.dequeueCell(with: ArtistSwayeCell.self, indexPath: indexPath)
            if userWatchlists.isEmpty {
                cell.staticData(row: indexPath.row)
            } else {
                cell.configureData(self.userWatchlists[indexPath.row])
            }
            return cell
        }
    }
    
    
    internal func getArtistHeaderCell(_ tableview: UITableView, section: Int) -> UIView {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ArtistHeaderCell") as? ArtistHeaderCell else {
            return UIView()
        }
        headerView.userBalance = self.userBalance
        headerView.populateData(section: section, screenType: .Home)
        
        if section == 0 {
           
            headerView.btnViewAllAction = {
                self.navigationItem.title = "Account"
                Router.shared.navigate(to: SummaryVC.self, storyboard: .Account, action: .push, animated: true, navigationController: .current) { () -> SummaryVC.RequiredParams in
                    return
                }
            }
        } else if section == 1 {
            
            headerView.btnViewAllAction = {
                 self.navigationItem.title = "Home"
                Router.shared.navigate(to: SwayeVC.self, storyboard: .Home, action: .push, animated: true, navigationController: .current) { () -> SwayeVC.RequiredParams in
                    return
                }
            }
        } else {
            
            headerView.btnViewAllAction = {
                self.navigationItem.title = "Home"
                Router.shared.navigate(to: WatchlistVC.self, storyboard: .Home, action: .push, animated: true, navigationController: .current) { () -> WatchlistVC.RequiredParams in
                    return
                }
            }
        }
        return headerView
    }
}
