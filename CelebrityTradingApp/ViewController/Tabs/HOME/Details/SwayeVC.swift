//
//  SwayeVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class SwayeVC: BaseVC, VCConfigurator {
    
    typealias RequiredParams = ()
    
    //MARK: IBOutlets and Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var shareNameValueLbl: UILabel!
    @IBOutlet weak var shareStatusContainerView: UIView!
    @IBOutlet weak var shareStatusLbl: UILabel!
    @IBOutlet weak var headerChart: LineChart!
    @IBOutlet weak var headerTimeCollection: UICollectionView!

    
    internal var artists = [UserPortofolio]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    internal var isAPICalled: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        self.headerTimeCollection.selectItem(at: selectedIndexPath, animated: false, scrollPosition: [])
        CTASocketManager.sharedInstance.subscribeToMakret()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.mainQueueAsync {
            self.getMySwaye()
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        
        self.registerNibs()

        headerTimeCollection.dataSource = self
        headerTimeCollection.delegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        
        //Adjust UITableView scroll inset
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
        self.tableView.contentInset = adjustForTabbarInsets
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMarketData), name: NSNotification.Name(rawValue: "updateMarketData"), object: nil)
    }

    
    private func headerSetup() {
        delay(0.1) {
            self.addGradientOnHeaderImage()
        }
        self.headerView.height = (screen_height*284)/812
        self.headerChart.backgroundColor = .clear
        self.shareNameValueLbl.text = "Swaye Value\n$63.6600"
        self.shareStatusLbl.text = "$3.6600 (1.5%)"
        self.shareStatusContainerView.backgroundColor = UIColor.AppColor.rrowsGreen2.withAlphaComponent(0.7)
        //TO_DO
        self.shareStatusContainerView.roundCorners([.topRight,.bottomRight], radius: 12)

        /*// Sample dataset
        let dataEntries = [PointEntry(value: 0, title: ""), PointEntry(value: 100, title: ""), PointEntry(value: 100, title: ""), PointEntry(value: 100, title: ""), PointEntry(value: 20, title: ""), PointEntry(value: 30, title: ""), PointEntry(value: 120, title: "")] */
        
        self.generateRandomDataToChart()
    }
    
    func addGradientOnHeaderImage() {
        let gradient = CAGradientLayer()
        gradient.frame = headerView.bounds
        gradient.colors = [UIColor.init(r: 86, g: 102, b: 126, alpha: 0.0).cgColor,UIColor.init(r: 14, g: 11, b: 31, alpha: 1.0).cgColor]
        headerImage.layer.addSublayer(gradient)
    }
    
     override func registerNibs() {
        super.registerNibs()
        self.tableView.registerCell(with: ArtistSwayeCell.self)
        self.tableView.registerHeaderFooter(with: ArtistHeaderCell.self)
    }

    func generateRandomDataToChart() {
        let dataEntries = generateChartEntries()
        headerChart.dataEntries = dataEntries
        headerChart.isCurved = false
    }

    //MARK: Populate data into chart
    private func generateChartEntries() -> [PointEntry] {
        var result: [PointEntry] = []
        for i in 0..<100 {
            let value = Int(arc4random() % 500)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM"
            var date = Date()
            date.addTimeInterval(TimeInterval(24*60*60*i))
            
            result.append(PointEntry(value: value, label: formatter.string(from: date)))
        }
        return result
    }

    //VCConfigurator Delegate Methos
    func configureWithParameters(param: (String?)) {
        return
    }
    
    @objc func updateMarketData(notification : NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            if let marketData = dict["marketData"] as? [MarketData], !marketData.isEmpty {
                for data in marketData {
                    if let index = self.artists.firstIndex(where: {$0.artist.symbol == data.CoinName}) {
                        self.artists[index].changeInPrice = data.ChangeInPrice
                        self.artists[index].swayePrice = data.CurrentTradingPrice
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
}

extension SwayeVC {
    
    func getMySwaye() {
        self.showSpinner()
        GraphQLServices.shared.getAllArtists(pageNo: 1, pageSize: 10, success: { (json) in
             self.removeSpinner()
             self.isAPICalled = true
             let artists = UserPortofolio.returnUserPortofolioListingArray(json[ApiKey.artists])
             self.artists = artists
        }) { (error) -> (Void) in
             self.isAPICalled = true
            self.removeSpinner()
            CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
    }
    
}
