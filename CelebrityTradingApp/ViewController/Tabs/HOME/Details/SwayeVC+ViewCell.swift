//
//  SwayeVC+ViewCell.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

extension SwayeVC {
    
    //MARK: Day Wise Share Status Cell
    internal func getDayShareStatusCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: DayWiseCell.self, indexPath: indexPath)
        
        cell.populateDate(item : indexPath.item)

        return cell
    }
    
    internal func getArtistShareCell(_ tableview: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableview.dequeueCell(with: ArtistSwayeCell.self, indexPath: indexPath)
        if artists.isEmpty {
            cell.staticData(row: indexPath.row)
        } else {
            cell.configurePortofolioData(self.artists[indexPath.row])
        }
        return cell
    }
    
    internal func getArtistHeaderCell(_ tableview: UITableView, section: Int) -> UIView {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ArtistHeaderCell") as? ArtistHeaderCell else {
            return UIView()
        }
        
        headerView.populateData(section: section, screenType: .Swaye)
        return headerView
    }
}
