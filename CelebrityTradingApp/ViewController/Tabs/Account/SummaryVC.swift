//
//  SummaryVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 04/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class SummaryVC: BaseVC, VCConfigurator {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var progressView: CircularProgressBar!
    @IBOutlet weak var tableView: UITableView!
    
    typealias RequiredParams = ()
    
    let labelArr = ["Cash: $00.00",
                    "Cash is the total amount of money you have available to purchase Swaye, including pending transfers.",
                    "Available for Withdrawl",
                    "Pending Transfer",
                    "Available Cash"]
    let priceArr = ["","","$0.0000","$25.0000","$0.0000"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    internal override func initialSetup() {
        self.headerView.height    = (screen_height*465)/812
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        //Adjust UITableView scroll inset
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height+20.0, right: 0)
        self.tableView.contentInset = adjustForTabbarInsets
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
        self.setupProgressView()
        self.navTitle = "Summary"
        self.addRightButtonToNavigation(image: UIImage(named: "icSearch"))
    }
    
    override func configureNavigationBar() {
        super.configureNavigationBar()
        let navbar = self.navigationController?.navigationBar
        navbar?.tintColor = .white
        self.setNavigationBarClear = true
        self.shouldChangeNavigationWhileScroll = true
        self.statusBarStyle = .lightContent
        self.navigationController?.navigationBar.titleTextAttributes = [
                   NSAttributedString.Key.foregroundColor: AppColors.clear]
       // self.navigationController?.navigationBar.installBlurEffect()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.configureNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.headerView.addGradientToHeaderView()
    }
    
    override func registerNibs() {
        self.tableView.registerCell(with: CashCell.self)
        self.tableView.registerCell(with: CashDescriptionCell.self)
        self.tableView.registerCell(with: CashRecordCell.self)
    }
    
    private func setupProgressView(){
        self.progressView.safePercent     = 100
        self.progressView.backgroundColor = .clear
        self.progressView.starttAngle     = (-CGFloat.pi/2)
        self.progressView.lineWidth       = (screen_width*20)/375
        self.progressView.labelPercent.isHidden = true
        self.progressView.label.isHidden        = true
        self.progressView.lineBackgroundColor   = UIColor.init(
            r: 245,
            g: 245,
            b: 245,
            alpha: 0.3)
        self.progressView.drawDropShadow(color: UIColor.AppColor.changeBlack)
        self.progressView.startGradientColor    = UIColor.init(
            r: 66,
            g: 147,
            b: 33,
            alpha: 1.0
        )
        self.progressView.endGradientColor      = UIColor.init(
            r: 180,
            g: 236,
            b: 81,
            alpha: 1.0)
        delay(1.0) {
            self.progressView.setProgress(to: 0.55, withAnimation: true)
        }
    }
}

//MARK:- UITableView DataSource and Delegate Method
extension SummaryVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.labelArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 55 : 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueCell(with: CashCell.self, indexPath: indexPath)
            return cell
        case 1:
            let cell = tableView.dequeueCell(with: CashDescriptionCell.self, indexPath: indexPath)
            return cell
        default:
            let cell = tableView.dequeueCell(with: CashRecordCell.self, indexPath: indexPath)
            cell.titleLbl.text = self.labelArr[indexPath.row]
            cell.priceLbl.text = self.priceArr[indexPath.row]
            return cell
        }
    }
}
