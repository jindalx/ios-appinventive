//
//  AccountVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class AccountVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var settingsArr = ["settingsName" :
        [LocalString.summaryTitle,
         LocalString.transfersTitle,
         LocalString.historyTitle,
         LocalString.statementsTitle,
         LocalString.settingsTitle,
         LocalString.helpTitle],
                                    "settingsDesc" :
                                        [LocalString.summaryDescTitle,
                                         LocalString.transfersDescTitle,
                                         LocalString.historyDescTitle,
                                         LocalString.statementsDescTitle,
                                         LocalString.settingsDescTitle,
                                         LocalString.helpDescTitle]]
    
    //Inset Calculations
    let left             = CGFloat(15)
    let right            = CGFloat(15)
    let top              = (screen_height*17)/812
    let bottom           = (screen_height*20)/812
    let interSpacing     = (screen_width*15)/375
    let cellWidth        = (screen_width*270)/375
    let cellHeight       = (screen_height*150)/812
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.headerContainerView.addGradientToHeaderView()
    }
    
    override func registerNibs() {
        super.registerNibs()
        self.tableView.registerCell(with: AccountCell.self)
    }
    
    private func intialSetup() {
        
        self.headerContainerView.height = (screen_height*335)/812
        
        self.collectionView.dataSource = self
        self.collectionView.delegate   = self
        
        self.tableView.dataSource = self
        self.tableView.delegate   = self
        self.tableView.tableFooterView = UIView()
        
        //Adjust UITableView scroll inset
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
        self.tableView.contentInset = adjustForTabbarInsets
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
        self.navTitle = "Account"
        self.navigationItem.title = self.navTitle
        self.addRightButtonToNavigation(image: UIImage(named: "icSearch"))
    }
    
    override func configureNavigationBar() {
        super.configureNavigationBar()
        self.setNavigationBarClear = true
        self.shouldChangeNavigationWhileScroll = true
        self.setNavigationBarClearWhileScroll = !(self.tableView.contentOffset.y / 150 > 0.3)
//        self.statusBarStyle = .lightContent
//        self.setNavigationTitleClear = true
    }
    
}

//MARK: UICollectionViewDataSource and Delegate Methods
//
extension AccountVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: DayWiseCell.self, indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cellWidth, height: self.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return self.interSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: self.top, left: self.left, bottom: self.bottom, right: self.right)
    }
    
}

//MARK: UITableViewDataSource and Delegate Methods
//
extension AccountVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: AccountCell.self, indexPath: indexPath)
        
        cell.titleLbl.text    = self.settingsArr["settingsName"]![indexPath.row]
        cell.subTitleLbl.text = self.settingsArr["settingsDesc"]![indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch ScreenToOpen(indexPath: indexPath as NSIndexPath) {
            
        case .Summary: 
            Router.shared.navigate(to: SummaryVC.self, storyboard: .Account, action: .push, animated: true, navigationController: .current) { () -> SummaryVC.RequiredParams in
                return
            }
        case .Transfer:
            Router.shared.navigate(to: TransfersVC.self, storyboard: .Account, action: .push, animated: true, navigationController: .current) { () -> TransfersVC.RequiredParams in
                return
            }
        default:
                print("Not found any controller at selcted IndexPath")
        }
    }
}

