//
//  TransfersVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 06/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class TransfersVC: BaseVC, VCConfigurator {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var linkedAccountContainerView: UIView!
    typealias RequiredParams = ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.headerView.height = (screen_height*385)/812
        self.headerView.addGradientToHeaderView()

        self.linkedAccountContainerView.drawDropShadow(color: #colorLiteral(red: 0.8823529412, green: 0.8823529412, blue: 0.8823529412, alpha: 1))
        
        //Adjust UITableView scroll inset
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
        self.tableView.contentInset = adjustForTabbarInsets
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
    }
    
    override func configureNavigationBar() {
        super.configureNavigationBar()
        let navbar = self.navigationController?.navigationBar
        navbar?.tintColor = .white
        self.setNavigationBarClear = true
    }
}
