//
//  MusicVC+Delegates.swift
//  CelebrityTradingApp
//
//  Created by Admin on 24/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//
import UIKit
import Foundation

extension MusicVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0 : return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return  self.getTopSongsTableCell(tableView, indexPath: indexPath)
        case 1 :
            return  self.getMusicTableCell(tableView, indexPath: indexPath)
        default:
            return  self.getVideoTableCell(tableView, indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueHeaderFooter(with: NewsHeaderView.self) 
        view.headerTitle.text = headerSectionArray[section]
        if section == 0 { view.seeMoreStackView.isHidden  =  false }
        else {view.seeMoreStackView.isHidden  =  true }
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return (70.0 * screen_height) / 812
        case 1:
            return (247.0 * screen_height) / 812
        default:
            return (246.0 * screen_height) / 812
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
    }
    
    
}
