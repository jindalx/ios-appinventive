//
//  MusicVC+TableCells.swift
//  CelebrityTradingApp
//
//  Created by Admin on 24/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import Foundation
import UIKit

//MARK:- TableViewCells
extension MusicVC {
    
    /// Get Title Cell
    internal func getMusicTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: MusicTableCell.self, indexPath: indexPath)
        cell.currentCell = .AlbumCell
        cell.clipsToBounds = true
        return cell
    }
    
    internal func getVideoTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: MusicTableCell.self, indexPath: indexPath)
        cell.currentCell = .VideoCell
        cell.clipsToBounds = true
        return cell
    }
    
    internal func getTopSongsTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(with: TopSongSTableCell.self, indexPath: indexPath)
        cell.topLineView.isHidden  =  indexPath.row == 0 ? false : true
        return cell
    }
    
    
}

