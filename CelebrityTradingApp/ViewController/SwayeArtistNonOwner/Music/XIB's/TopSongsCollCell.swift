//
//  TopSongsCollCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 28/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class TopSongsCollCell: UICollectionViewCell {
    
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var topSongImgView: UIImageView!
    @IBOutlet weak var songNameLbl: UILabel!
    @IBOutlet weak var SongAlbumLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.topSongImgView.layer.cornerRadius = 4.0
    }

}
