//
//  VideoCollCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 24/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class VideoCollCell: UICollectionViewCell {

    @IBOutlet weak var videoImgView: UIImageView!
    @IBOutlet weak var videiTitleLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.videoImgView.layer.cornerRadius = 4.0
    }

}
