//
//  TopSongSTableCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 28/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit


class TopSongSTableCell: UITableViewCell {
    
      private var itemWidth: CGFloat = 0.0
      private var currentItem: Int = 0
    
    
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var topSongsCollView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerXIB()

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.CollectionViewSetUp()
    }
    
    
    private func registerXIB(){
        self.topSongsCollView.registerCell(with: TopSongsCollCell.self)
        self.flowLayoutSetup()
        
    }
    
    
    func flowLayoutSetup() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        itemWidth = screen_width - 34
        layout.scrollDirection = .horizontal
        topSongsCollView.collectionViewLayout = layout
        topSongsCollView?.decelerationRate = UIScrollView.DecelerationRate.normal
        
    }
    
    private func CollectionViewSetUp() {
        self.topSongsCollView.delegate = self
        self.topSongsCollView.dataSource = self
    }
    
    
}


extension TopSongSTableCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: TopSongsCollCell.self, indexPath: indexPath)
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: collectionView.bounds.width - 34.0 , height: collectionView.bounds.height)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
            return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
       
            return 9.0
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let paddingInset: CGFloat = 15.0
        return UIEdgeInsets(top: 0, left: paddingInset, bottom: 0, right: paddingInset)
    }
    
    
}

extension TopSongSTableCell : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.manageScroll(scrollView)
    }
    func manageScroll(_ scrollView: UIScrollView) {
        if scrollView === self.topSongsCollView {
            let offset = scrollView.contentOffset
            let page = offset.x / self.bounds.width
            self.currentItem = Int(page)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageWidth = Float(itemWidth + 9)
        let targetXContentOffset = Float(targetContentOffset.pointee.x)
        let contentWidth = Float(topSongsCollView.contentSize.width)
        var newPage : Float = 0
        
        if velocity.x == 0 {
            newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
        }
        else {
            newPage = Float(velocity.x > 0 ? self.currentItem + 1 : self.currentItem - 1)
            if newPage < 0 {
                newPage = 0
            }
            if (newPage > contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }
        
        self.currentItem = Int(newPage)
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
        
        if Int(newPage) < 3 {
            //  lblTier.text =  "TIER \(Int(newPage) + 1)/\(totalLevel)"
        }
    }
}
