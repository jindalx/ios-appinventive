//
//  MusicTableCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 24/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class MusicTableCell: UITableViewCell {
    
    private var itemWidth: CGFloat = 0.0
    private var currentItem: Int = 0
    
    
    
    enum CellContents {
        case AlbumCell , VideoCell
    }
    
    
    var currentCell: CellContents = .AlbumCell
    
    @IBOutlet weak var cardCollBottomConst: NSLayoutConstraint!
    @IBOutlet weak var cardCollTopConst: NSLayoutConstraint!
    @IBOutlet weak var cardCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureUI()
    }
    
    /// Call to configure ui
    private func configureUI() {
        self.cardCollectionView.registerCell(with: AlbumCollCell.self)
        self.cardCollectionView.registerCell(with: VideoCollCell.self)
        self.flowLayoutSetup()
        self.cardCollectionView.delegate = self
        self.cardCollectionView.dataSource = self
    }
    
    ///Call to populates data
    func configureCell() {
        
        self.cardCollTopConst.constant = 0.0
        self.cardCollBottomConst.constant = 0.0
    }
    
    func flowLayoutSetup() {
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            itemWidth = screen_width - 35
            layout.scrollDirection = .horizontal
            cardCollectionView.isPagingEnabled = false
            cardCollectionView.collectionViewLayout = layout
            cardCollectionView?.decelerationRate = UIScrollView.DecelerationRate.normal
        
    }

    ///Get Gallery
    private func getAlbumCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: AlbumCollCell.self, indexPath: indexPath)
        return cell
    }
    
    ///Get Video
    private func getVideoCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(with: VideoCollCell.self, indexPath: indexPath)
        return cell
    }
    
}



//MARK:- UICollectionView Extensions
extension MusicTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.currentCell {
        case .AlbumCell:
            return 4
        case .VideoCell:
            return 4
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch self.currentCell {
        case .AlbumCell:
            return self.getAlbumCell(collectionView, indexPath: indexPath)
        case .VideoCell:
            return self.getVideoCell(collectionView, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch self.currentCell {
        case .AlbumCell:
            return CGSize(width: (collectionView.bounds.width - 20) / 2 , height:  collectionView.bounds.height)
        case .VideoCell:
            return CGSize(width: collectionView.bounds.width - 35 , height: collectionView.bounds.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        switch self.currentCell {
        case .AlbumCell :
            return 0.0
        case .VideoCell :
            return 0.0
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        switch self.currentCell {
            
        case .AlbumCell :
            return 0.0
        case .VideoCell :
            return 10.0
        }
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        var paddingInset: CGFloat = 0.0
        
        switch self.currentCell {
        case .AlbumCell :
            paddingInset =  10.0
        case .VideoCell :
            paddingInset =  15.0
        }
        
        return UIEdgeInsets(top: 0, left: paddingInset, bottom: 0, right: paddingInset)
    }
}



extension MusicTableCell {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.manageScroll(scrollView)
    }
    func manageScroll(_ scrollView: UIScrollView) {
        
        if scrollView === self.cardCollectionView {
            let offset = scrollView.contentOffset
            let page = offset.x / self.bounds.width
            self.currentItem = Int(page)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        guard self.currentCell == .VideoCell else { return }
        
        let pageWidth = Float(itemWidth + 10)
        let targetXContentOffset = Float(targetContentOffset.pointee.x)
        let contentWidth = Float(cardCollectionView.contentSize.width)
        var newPage : Float = 0
        
        if velocity.x == 0 {
            newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
        }
        else {
            newPage = Float(velocity.x > 0 ? self.currentItem + 1 : self.currentItem - 1)
            if newPage < 0 {
                newPage = 0
            }
            if (newPage > contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }
        
        self.currentItem = Int(newPage)
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
    }
}

