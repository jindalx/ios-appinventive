//
//  MusicVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 24/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class MusicVC: UIViewController {
    

    //MARK:- PEOPERTIES
    //================
    var headerSectionArray = ["Top Songs","Album","Videos"]
    
    //MARK:- IBOUTLETS
    //================
    
    @IBOutlet weak var mainTableView: UITableView!
    
    
    
    //MARK:- VIEW LIFE CYCLE
    //================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    
    
    
    //MARK:- PRIVATE FUNCTIONS
    //=====================
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.registerNibs()
        self.footerViewSetUp()
    }
    
    
    
    private func tableViewSetUp() {
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
    }
    
    private func footerViewSetUp() {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: screen_width, height: 150.0))
        self.mainTableView.tableFooterView?.height = 150.0
        self.mainTableView.tableFooterView = view
        
       
    }
       
    
    private func registerNibs() {
        self.mainTableView.registerCell(with: MusicTableCell.self)
        self.mainTableView.registerCell(with: TopSongSTableCell.self)
        self.mainTableView.registerHeaderFooter(with: NewsHeaderView.self)
               
    }
    
    
    
    //MARK:- IBACTIONS
    //=====================
    
}
