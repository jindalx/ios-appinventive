//
//  StatsProgressCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit
import GradientProgressBar

class StatsProgressCell: UITableViewCell {
    
    @IBOutlet weak var buyProgressView: GradientProgressBar!
    @IBOutlet weak var holdProgressView: GradientProgressBar!
    @IBOutlet weak var sellProgressView: GradientProgressBar!
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.holdProgressView.gradientColors = UIColor.AppColor.progressBarColors
        self.buyProgressView.gradientColors = [UIColor.AppColor.changeGray,UIColor.AppColor.changeGray]
        self.sellProgressView.gradientColors = [UIColor.AppColor.changeGray,UIColor.AppColor.changeGray]
            
        delay(2.0) {
            self.buyProgressView.setProgress(0.30, animated: true)
            self.holdProgressView.setProgress(0.60, animated: true)
            self.sellProgressView.setProgress(0.08, animated: true)
        }
    }

    
}
