//
//  BuyRatingCollCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class BuyRatingCollCell: UICollectionViewCell {

    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var textLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textLbl.numberOfLines = Int(3.0)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.mainContainerView.addShadow(cornerRadius: 5, color: UIColor.black.withAlphaComponent(0.07), offset: CGSize(width: 0.5, height: 0.5), opacity: 1, shadowRadius: 5)
    }

}
