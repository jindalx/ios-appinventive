//
//  StatsGraphCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import Charts

class StatsGraphCell: UITableViewCell, ChartViewDelegate {
    
    //MARK:- IBOUTLETS
    //================
    
    @IBOutlet weak var chartView: LineChartView!
  //  @IBOutlet weak var statsGraphView: LineChart!
    @IBOutlet var graphButtons: [UIButton]!
    
    @IBOutlet weak var lineChartView: LineChart!
    
    private var dataEntry = [ChartDataEntry]()
    private var tempDtaEntry: ChartDataEntry?
    private var yPos: CGFloat?
    
    var touchLocation: CGPoint?
    
    var customLabel = UILabel()
    var anotherLabel = UILabel()
    var lastLabel = UILabel()
    
    //MARK:- VIEW LIFE CYCLE
    //================
    override func awakeFromNib() {
        super.awakeFromNib()
        self.graphButtonSetUp()
       // self.statsGraphView.backgroundColor = .clear
        //let dataEntries = generateChartEntries()
       // statsGraphView.dataEntries = dataEntries
       // statsGraphView.isCurved = false
        self.setupChart()
        self.createLabel()
    }
    
    
    //MARK:- PRIVATE FUNCTIONS
    //================
    //MARK: Populate data into chart
    private func generateChartEntries() -> [PointEntry] {
        var result: [PointEntry] = []
        for i in 0..<100 {
            let value = Int(arc4random() % 500)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM"
            var date = Date()
            date.addTimeInterval(TimeInterval(24*60*60*i))
            
            result.append(PointEntry(value: value, label: formatter.string(from: date)))
        }
        return result
    }
    
    
    private func graphButtonSetUp() {
        self.graphButtons.forEach { (button) in
            self.graphButtons[button.tag].backgroundColor = .white
            self.graphButtons[button.tag].layer.cornerRadius = 4.0
        }
        self.graphButtons[0].backgroundColor = UIColor.init(r: 1, g: 183, b: 203, alpha: 0.45)
    }
    
    private func setupChart() {
        
        chartView.addBottomBorderWithColor(color: UIColor.AppColor.changeBlack, width: 0.5)
        self.lineChartView.sendSubviewToBack(self.chartView)
       // self.lineChartView.backgroundColor = UIColor.AppColor.lightTealColor
       // self.lineChartView.backgroundColor = .clear
        generateRandomDataToChart()
        chartView.delegate = self
        // chartView.chartDescription?.enabled = false
        chartView.dragEnabled = true
        chartView.setScaleEnabled(false)
        // chartView.pinchZoomEnabled = true
        
        chartView.leftAxis.drawAxisLineEnabled = false
        chartView.leftAxis.drawGridLinesEnabled = false
        chartView.leftAxis.gridColor = UIColor.clear
        chartView.rightAxis.drawAxisLineEnabled = false
        chartView.rightAxis.drawGridLinesEnabled = false
        
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.backgroundColor = .clear
        chartView.translatesAutoresizingMaskIntoConstraints = false
        
        chartView.tintColor = .black
        //chartView.//
        chartView.drawGridBackgroundEnabled = false
        chartView.gridBackgroundColor = .white
        let l = chartView.legend
        l.form = .circle
        l.textColor = .white
        l.horizontalAlignment = .left
        l.verticalAlignment = .center
        l.orientation = .horizontal
        l.drawInside = false
        
        let xAxis = chartView.xAxis
        xAxis.labelFont = .systemFont(ofSize: 11)
        xAxis.labelTextColor = .white
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelTextColor = UIColor.white
        leftAxis.axisMaximum = 200
        leftAxis.axisMinimum = 0
        leftAxis.drawGridLinesEnabled = false
        leftAxis.granularityEnabled = false
        
        let rightAxis = chartView.rightAxis
        rightAxis.labelTextColor = .white
        rightAxis.axisMaximum = 400
        rightAxis.axisMinimum = -200
        rightAxis.granularityEnabled = true
        
        self.chartView.xAxis.drawGridLinesEnabled = false
        self.chartView.leftAxis.drawLabelsEnabled = false
        self.chartView.legend.enabled = false
  
        chartView.animate(xAxisDuration: 0.8)
        self.setChartValues()
    }
    
    func generateRandomDataToChart() {
        let dataEntries = generateChartEntries()
        lineChartView.dataEntries = dataEntries
        lineChartView.isCurved = false
    }
    
    func setChartValues(_ count: Int = 20) {
        let values = (0..<count).map{ (i) -> ChartDataEntry in
            let val = Double(arc4random_uniform(UInt32(count)) + 5)
            return ChartDataEntry(x: Double(i), y: val*8)
        }
        
        self.dataEntry = values
        let set1 = LineChartDataSet(entries: values, label: "chart")
        set1.valueColors = [AppColors.appBlueDark]
        set1.drawCirclesEnabled = false
        set1.mode = .horizontalBezier
        
//        let gradientColors = [UIColor.AppColor.graphBackground.cgColor] as CFArray // Colors of the gradient
//        let colorLocations:[CGFloat] = [1.0, 0.0] // Positioning of the gradient
//        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations) // Gradient Object
//        set1.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0) // Set the Gradient
//        set1.drawFilledEnabled = true
        set1.mode = .linear
        set1.colors = [AppColors.appBlueDark]
        set1.cubicIntensity = .ulpOfOne
        set1.lineWidth = 3.0
        let data = LineChartData(dataSet: set1)
        
        data.setDrawValues(false)
        
        chartView.xAxis.axisLineColor = UIColor.blue
        chartView.rightAxis.axisLineColor = UIColor.black
        self.chartView.data = data
        
        print(self.chartView.frame.origin)
        self.createLabel()
        
    }
    
    
    private func createLabel() {
        
        // self.customLabel.frame =  CGRect(x: Double(UIScreen.main.bounds.width - 60), y: Double((self.yPos ?? 0.0) - 6.0), width: 58.0, height: 24.0)
        customLabel.text = "90.76"
        anotherLabel.text = "hjh"
        anotherLabel.backgroundColor = .red
        self.contentView.addSubview(anotherLabel)
        customLabel.font = UIFont.systemFont(ofSize: 14)
        customLabel.textAlignment = .center
        customLabel.backgroundColor = .clear
        
        var position = CGPoint()
        position.x = CGFloat(self.dataEntry[dataEntry.count - 1].x) // index on x axis, like 0,1,2
        position.y = CGFloat(self.dataEntry[dataEntry.count - 1].y)
        
        // position = CGPointApplyAffineTransform(position, chartView.getTransformer(forAxis: .right))
        let vsal =  chartView.getTransformer(forAxis: .right).pixelToValueMatrix
        position = __CGPointApplyAffineTransform(position, vsal)
        
        position.x = CGFloat(self.chartView.frame.width)
        self.lastLabel.frame = CGRect(origin: position, size: CGSize(width: 60.0, height: 30.0))
        
        lastLabel.backgroundColor = .black
        
       // self.contentView.addSubview(lastLabel)
    }
    
    func chartView(_ chartView: ChartViewBase, animatorDidStop animator: Animator) {
        self.chartView.highlightValue(x: self.dataEntry[dataEntry.count - 1].x, y: dataEntry[dataEntry.count - 1].y, dataSetIndex: 1)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.contentView)
        self.yPos = location.y
        print("touch location", location.x, location.y)
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        
        DispatchQueue.mainQueueAsync {
            self.anotherLabel.frame = CGRect(x: CGFloat(self.chartView.frame.width - 60), y: highlight.yPx + 40.0, width: 80, height: 30)
            self.anotherLabel.text = "\(entry.y)"
        }
        
        self.customLabel.frame.origin.y = self.yPos ?? 0.0
        self.customLabel.backgroundColor = .orange
        print("chart location", chartView.frame.origin.x, chartView.frame.origin.y)
        let set1 = LineChartDataSet(entries: self.dataEntry, label: "chart")
        set1.drawCirclesEnabled = false
        set1.mode = .linear
        
//        let gradientColors = [UIColor.AppColor.graphBackground.cgColor] as CFArray // Colors of the gradient
//        let colorLocations:[CGFloat] = [1.0, 0.0] // Positioning of the gradient
//        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations) // Gradient Object
//        set1.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0) // Set the Gradient
//        set1.drawFilledEnabled = true
        set1.valueColors = [AppColors.appBlueDark]
        set1.colors = [AppColors.appBlueDark]
        set1.lineWidth = 3.0
        
        
        let entrydata = LineChartData(dataSet: set1)
        
        entrydata.setDrawValues(false)
        
        let data = LineChartData(dataSet: set1)
        self.chartView.data = data
        
        if tempDtaEntry != entry {
            self.tempDtaEntry = entry
            let set = LineChartDataSet(entries: [entry], label: "chart")
            
            self.chartView.lineData?.addDataSet(set)
            
            set1.setDrawHighlightIndicators(true)
            set1.drawVerticalHighlightIndicatorEnabled = true
            set1.highlightLineDashLengths = [8.0, 4.0]
            set1.highlightLineWidth = 3.0
            set1.highlightColor = AppColors.graphHorizontalColor
            set1.circleHoleColor = .brown
            set1.setCircleColor(.red)
            set1.circleColors = [.black]
        } else {
            let set = LineChartDataSet(entries: [entry], label: "chart")
            self.chartView.lineData?.removeDataSet(set)
        }
        
        self.chartView.xAxis.axisLineColor = UIColor.red
        self.chartView.rightAxis.axisLineColor = UIColor.black
       
    }
    
    //MARK:- IBACTIONS
    //================
    
    @IBAction func graphBtnActions(_ sender: UIButton) {
        self.graphButtons.forEach { (button) in
            if button.tag == sender.tag {self.graphButtons[button.tag].backgroundColor = UIColor.init(r: 1, g: 183, b: 203, alpha: 0.45)}
            else { self.graphButtons[button.tag].backgroundColor = .white
            }
        }
        self.contentView.layoutIfNeeded()
    }
    
    
}
