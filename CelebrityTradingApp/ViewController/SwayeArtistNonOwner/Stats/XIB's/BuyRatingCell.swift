//
//  BuyRatingCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class BuyRatingCell: UITableViewCell {
    
    private var itemWidth: CGFloat = 0.0
    private var currentItem: Int = 0
    

    @IBOutlet weak var ratingCollView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ratingCollView.delegate = self
        self.ratingCollView.dataSource = self
        self.ratingCollView.registerCell(with: BuyRatingCollCell.self)
        self.flowLayoutSetup()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func flowLayoutSetup() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        itemWidth = screen_width - 35
        layout.scrollDirection = .horizontal
        ratingCollView.isPagingEnabled = false
        ratingCollView.collectionViewLayout = layout
        ratingCollView?.decelerationRate = UIScrollView.DecelerationRate.normal
        
        
    }
    
    
    
    
}

extension BuyRatingCell : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BuyRatingCollCell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.ratingCollView.frame.width - 35.0 , height: self.ratingCollView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          
        return UIEdgeInsets(top: 0, left: 12.5 , bottom: 0, right: 12.5)
      }
      
  
    
    
}

extension BuyRatingCell {
    
       func manageScroll(_ scrollView: UIScrollView) {
           if scrollView === self.ratingCollView {
               let offset = scrollView.contentOffset
               let page = offset.x / self.bounds.width
               self.currentItem = Int(page)
           }
       }
       
       func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
           
          
           
           let pageWidth = Float(itemWidth + 10)
           let targetXContentOffset = Float(targetContentOffset.pointee.x)
           let contentWidth = Float(ratingCollView.contentSize.width)
           var newPage : Float = 0
           
           if velocity.x == 0 {
               newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
           }
           else {
               newPage = Float(velocity.x > 0 ? self.currentItem + 1 : self.currentItem - 1)
               if newPage < 0 {
                   newPage = 0
               }
               if (newPage > contentWidth / pageWidth) {
                   newPage = ceil(contentWidth / pageWidth) - 1.0
               }
           }
           
           self.currentItem = Int(newPage)
           let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
           targetContentOffset.pointee = point
       }
    
}
