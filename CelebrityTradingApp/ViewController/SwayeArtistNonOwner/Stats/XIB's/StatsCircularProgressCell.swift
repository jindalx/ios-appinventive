//
//  StatsCircularProgressCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 27/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit


class StatsCircularProgressCell: UITableViewCell {

   
    @IBOutlet weak var progressView: CircularProgressBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        progressView.safePercent = 100
        progressView.starttAngle = (-CGFloat.pi/2)
        progressView.startGradientColor = UIColor.init(r: 1, g: 183, b: 203, alpha: 1.0)
        progressView.endGradientColor = UIColor.init(r: 33, g: 100, b: 171, alpha: 1.0)
        self.progressView.drawDropShadow(color: UIColor.AppColor.progressBarShadow)
        self.progressView.lineBackgroundColor = UIColor.AppColor.backgroundGrey3
        
        self.progressView.lineWidth = (screen_width*20)/375
        delay(1.0) {
            self.progressView.setProgress(to: 0.65, withAnimation: true)
        }
    }
}
