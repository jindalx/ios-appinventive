//
//  StatsVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class StatsVC: UIViewController {
    
    //MARK:- IBOUTLETS
    //==================
    @IBOutlet weak var mainTableView: UITableView!
    
     var artistSymbol: String = "ARIG"
     var userType: HomeUserType = .swaye
    
    //MARK:- VIEW LIFE CYCLE
    //==================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    
    
    //MARK:- PRIVATE FUNCTIONS
    //==================
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.footerViewSetUp()
        self.registerXib()
    }
    
    private func tableViewSetUp() {
        self.mainTableView.delegate    = self
        self.mainTableView.dataSource  = self
        self.mainTableView.rowHeight = UITableView.automaticDimension
        self.mainTableView.estimatedRowHeight = 300
    }
    
    private func footerViewSetUp() {
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 181.0))
        self.mainTableView.tableFooterView?.height = 181.0
        self.mainTableView.tableFooterView = footerView
    }
    
    private func registerXib(){
        self.mainTableView.registerCell(with: StatsCircularProgressCell.self)
        self.mainTableView.registerCell(with: StatsProgressCell.self)
        self.mainTableView.registerCell(with: BuyRatingCell.self)
        self.mainTableView.registerCell(with: StatsGraphCell.self)
    }
    
    
}
//MARK:- TABLEVIEW DELEGATE AND DATA SOURCE
//==================

extension StatsVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userType == .swaye ? 4 : 3 // for swaye it will be 4 and 2 for watchlist
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableViewCellForStats(tableView, cellForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewHeightForStats(tableView, heightForRowAt: indexPath)
    }
    
    //Internal functions
     internal  func tableViewCellForStats(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        if self.userType == .swaye  {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueCell(with: StatsGraphCell.self)
                return cell
            case 1:
                let cell = tableView.dequeueCell(with: StatsCircularProgressCell.self)
                return cell
            case 2:
                let cell = tableView.dequeueCell(with: StatsProgressCell.self)
                return cell
            default:
               let cell = tableView.dequeueCell(with: BuyRatingCell.self)
               return cell
            }
        } else {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueCell(with: StatsGraphCell.self)
                return cell
            case 1:
                let cell = tableView.dequeueCell(with: StatsProgressCell.self)
                return cell
            default:
                let cell = tableView.dequeueCell(with: BuyRatingCell.self)
                return cell
            }
        }
    }
    
    internal func tableViewHeightForStats(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if userType == .swaye {
            switch indexPath.row {
            case 0: return 300.0
            case 1: return (275 * screen_height ) / 812
            case 2: return UITableView.automaticDimension
            default:
                return 160.0
            }
        } else {
            switch indexPath.row {
            case 0: return 300.0
            case 1: return UITableView.automaticDimension
            default:
                return 160.0
            }
        }
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        let touch = touches.first!
        let location = touch.location(in: self.view)
       // self.yPos = location.y
    }
    
}
