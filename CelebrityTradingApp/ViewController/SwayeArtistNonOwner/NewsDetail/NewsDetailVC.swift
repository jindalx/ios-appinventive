//
//  NewsDetailVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 28/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController {
    
    
    var newsDetailHeaderView = NewsDetailHeaderView.instanciateFromNib()

    var artistSymbol: String = "ARIG"
      
      private var news = [News]() {
          didSet {
              mainTableView.reloadData()
          }
      }
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    
    //MARK:- PRIVATE FUNCTIONS
    //==================
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.registerXib()
        self.headerViewSetUp()
      
    }
    
    private func tableViewSetUp() {
        self.mainTableView.delegate    = self
        self.mainTableView.dataSource  = self
    }
    
    private func headerViewSetUp() {
        self.headerView.height = 381.0
         self.headerView.width = screen_width
         self.newsDetailHeaderView.frame = CGRect.init(x: 0, y: 0, width: self.headerView.width, height: self.headerView.height)
         self.headerView.addSubview(newsDetailHeaderView)
     }
    
  
    
    private func registerXib(){
        self.mainTableView.registerCell(with: NewsDetailTableCell.self)
        
    }
    
    
}
//MARK:- TABLEVIEW DELEGATE AND DATA SOURCE
//==================

extension NewsDetailVC : UITableViewDelegate, UITableViewDataSource {
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueCell(with: NewsDetailTableCell.self)
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
}

