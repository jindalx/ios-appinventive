//
//  NewsDetailHeaderView.swift
//  CelebrityTradingApp
//
//  Created by Admin on 28/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import Foundation
//
//  ArtistHeaderView.swift
//  CelebrityTradingApp
//
//  Created by Admin on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//
import UIKit
import Foundation

class NewsDetailHeaderView: UIView {
    
  
    
    //MARK:- IBOUTLETS
    //==================
    
    
    @IBOutlet weak var imgContainerView: UIView!
    @IBOutlet weak var headerImgBtmConst: NSLayoutConstraint!
    @IBOutlet weak var buyArtistView: UIView!
    @IBOutlet weak var confirmArtistView: UIView!
    @IBOutlet weak var headerbackgroundView: UIView!
    @IBOutlet weak var headerImgView: UIImageView!
    @IBOutlet weak var availCashLbl: UILabel!
    @IBOutlet weak var feeLbl: UILabel!
    @IBOutlet weak var confirmArtistSubTitleLbl: UILabel!
    
    
  
    
    override func setNeedsLayout() {
        super.setNeedsLayout()
    }
    
    //MARK:- VIEW LIFE CYCLE
    //=====================
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetUp()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }


    class func instanciateFromNib() -> NewsDetailHeaderView {
        return Bundle .main .loadNibNamed("NewsDetailHeaderView", owner: self, options: nil)![0] as! NewsDetailHeaderView
    }
    
    
    
    //MARK:- PRIVATE FUNCTIONS
    //=======================
    
    private func initialSetUp() {
//        self.headerImgSetUp()
        
    }
    
   
    
}
