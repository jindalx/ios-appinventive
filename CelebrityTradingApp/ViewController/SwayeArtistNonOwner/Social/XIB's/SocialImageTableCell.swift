//
//  SocialImageTableCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 27/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class SocialImageTableCell: UITableViewCell {

    @IBOutlet weak var socialImgView: UIImageView!
    @IBOutlet weak var socialTimeLbl: UILabel!
    @IBOutlet weak var socialUrlLbl: UILabel!
    @IBOutlet weak var socialTitleLbl: UILabel!
    
    
    func populateData(model: SocialMedia) {
//        self.socialImgView.setImage(withUrl: model.thumbnail, placeholderImage: nil)
//        self.socialTitleLbl.text = model.content
//        self.socialUrlLbl.text = model.postUrl
//        self.socialTimeLbl.text = model.publishedTime
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.socialImgView.layer.cornerRadius = 4.0
    }

}
