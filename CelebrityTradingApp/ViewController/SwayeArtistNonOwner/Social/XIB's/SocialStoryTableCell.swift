//
//  SocialStoryTableCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 27/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class SocialStoryTableCell: UITableViewCell {

    @IBOutlet weak var socialBorderView: CircularProgressBar!
    @IBOutlet weak var socialProfileImgView: UIImageView!
    @IBOutlet weak var socialStoryLbl: UILabel!
    @IBOutlet weak var socialTimeLbl: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.socialProfileImgView.roundCornersWithBorder(weight: 0.0, borderColor: .clear)
    }
    
    
    func populateData(model: SocialMedia) {
        self.socialTimeLbl.text = model.publishedTime
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        socialBorderView.safePercent = 100
        socialBorderView.lineWidth = 5.0
        socialBorderView.starttAngle = (-CGFloat.pi/15)
        socialBorderView.endGradientColor = UIColor.init(r: 33, g: 100, b: 171, alpha: 1.0)
        socialBorderView.startGradientColor = UIColor.init(r: 1, g: 183, b: 203, alpha: 1.0)
        socialBorderView.lineBackgroundColor = UIColor.clear
        delay(2.0) {
            self.socialBorderView.setProgress(to: 0.75, withAnimation: true)
        }
      
       
    }

    
}
