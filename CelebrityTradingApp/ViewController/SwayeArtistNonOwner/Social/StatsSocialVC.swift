//
//  StatsSocialVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 27/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class StatsSocialVC: UIViewController {
    
    //MARK:- IBOUTLETS
      //==================

    @IBOutlet weak var mainTableView: UITableView!
    
    var artistId: String = "1"
    
    private var socialMedia = [SocialMedia]() {
        didSet {
            mainTableView.reloadData()
        }
    }
    
    var socialTitleArray = ["Jessie Reyez to Join Billie Eilish on 'Where Do We Go?' World Tour","Billie’s premiere performance of “No Time To Die” will be at the 2020 #BRITs on 2/18. Billie will be accompanied by @FINNEAS…."]
    //MARK:- VIEW LIFE CYCLE
    //==================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
        self.getSocialMedia()
    }
    //MARK:- PRIVATE FUNCTIONS
    //==================
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.footerViewSetUp()
        self.registerXib()
    }
    
    private func tableViewSetUp() {
        self.mainTableView.delegate    = self
        self.mainTableView.dataSource  = self
    }
    
    private func footerViewSetUp() {
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 181.0))
        self.mainTableView.tableFooterView = footerView
    }
    
    private func registerXib(){
        self.mainTableView.registerCell(with: SocialImageTableCell.self)
        self.mainTableView.registerCell(with: SocialTableCell.self)
        self.mainTableView.registerCell(with: SocialStoryTableCell.self)
        self.mainTableView.registerHeaderFooter(with: NewsHeaderView.self)
        
    }
    
    
}
//MARK:- TABLEVIEW DELEGATE AND DATA SOURCE
//==================

extension StatsSocialVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if self.socialMedia.endIndex > 0 {
//           if self.socialMedia[indexPath.row].thumbnail.isEmpty {
//                let cell = tableView.dequeueCell(with: SocialTableCell.self)
//                cell.socialTitleLbl.text = socialTitleArray[indexPath.row]
//                return cell
//            } else {
//                let cell = tableView.dequeueCell(with: SocialStoryTableCell.self)
//                cell.populateData(model: self.socialMedia[indexPath.row])
//                return cell
//            }
//        } else { return UITableViewCell()}
        switch indexPath.row {
        case 0,1:
             let cell = tableView.dequeueCell(with: SocialTableCell.self)
            cell.socialTitleLbl.text = socialTitleArray[indexPath.row]
            return cell
        case 2:
             let cell = tableView.dequeueCell(with: SocialStoryTableCell.self)
             return cell
        default:
             let cell = tableView.dequeueCell(with: SocialImageTableCell.self)
             return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueHeaderFooter(with: NewsHeaderView.self)
        view.headerTitle.text = "Social Media"
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0,1:
            return UITableView.automaticDimension
        case 2:
            return (200 * screen_height) / 812
        default:
            return (364 * screen_height) / 812
        }
        //        if self.socialMedia.endIndex > 0 {
        //            if !self.socialMedia[indexPath.row].thumbnail.isEmpty {
        //                return (200 * screen_height) / 812
        //            } else {
        //                return UITableView.automaticDimension
        //            }
        //        } else {
        //            return UITableView.automaticDimension
        //        }
        //
    }
}


//MARK: - API Calling
extension StatsSocialVC {
    
    func getSocialMedia() {
        GraphQLServices.shared.getSocialMedia(artistId: self.artistId, success: { (json) in
            let socialMedia = SocialMedia.returnSocialListingArray(json[ApiKey.list])
            self.socialMedia = socialMedia
        }) { (error) -> (Void) in
             CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
    }
}
