//
//  SwayeOtherUserVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//
import MXParallaxHeader
import UIKit

class SwayeOtherUserVC: BaseVC {

    //MARK:- PROPERTIES
    //===================
    var maxValue: CGFloat = 1.0
    var minValue: CGFloat = 0.0
    var finalMaxValue: Int = 0
    var currentProgress: CGFloat = 0
    var currentProgressIntValue: Int = 0
    var isScrollingFirstTime: Bool = true
    
    var userType: HomeUserType = .swaye
    var headerView = OtherArtistHeaderView.instanciateFromNib()
    var detailView = OtherArtistDetail.instanciateFromNib()
  
    var statsVC    : StatsVC!
    var newsVC     : NewsVC!
    var eventsVC   : EventsVC!
    var musicVC    : MusicVC!
    var socialVC   : StatsSocialVC!
    
    
    var userPortFolio : UserPortofolio?
    var artistDetail : Artist?
    var artistSymbol: String = "ARIG"
    private var scrollToTop: Bool = false
    
    //MARK:- IBOUTLETS
    //====================
    @IBOutlet weak var mainScrollView: MXScrollView!
 
    //MARK:- VIEW LIFE CYCLE
    //====================
    override func viewDidLoad() {
        super.viewDidLoad()
        let musicButtonType: MusicButtonType = self.userType == .swaye ? MusicButtonType.DETAIL : MusicButtonType.HOME
        NotificationCenter.default.post(name: Notification.Name(rawValue: "changeMusicButton"), object: nil, userInfo: ["musicButtonType": musicButtonType, "isHide": false])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        musicButtonType = .DETAIL

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let tabBarHeight    = (self.tabBarController?.tabBar.frame.size.height == nil) ? 70 : (self.tabBarController!.tabBar.frame.size.height)
        let statusBarHeight = view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        var frame = CGRect(x: 0, y: 0, width: screen_width, height: screen_height)
        self.mainScrollView.frame = frame
        self.mainScrollView.contentSize = frame.size
        frame = CGRect(x: 0, y: 0, width: screen_width, height: screen_height)
        frame.size.height = frame.size.height - (self.mainScrollView.parallaxHeader.minimumHeight + tabBarHeight + statusBarHeight)
        self.detailView.frame = frame
        scrollFrameSetup()
        
    }
    
    //MARK:- PRIVATE FUNCTIONS
    //====================
    
    override func initialSetup() {
        super.initialSetup()
        self.parallelHeaderSetUp()
        self.HeaderDataSetUp()
        self.instantiateViewController()
        self.addButtonOnRight()
        self.getArtistDetail()
    }
    
    override func configureNavigationBar() {
        super.configureNavigationBar()
        if !scrollToTop {
            let navbar = self.navigationController?.navigationBar
            navbar?.tintColor = .white
            self.setNavigationBarClear = true
            self.statusBarStyle = .lightContent
        }
    }
    
    private func parallelHeaderSetUp() {
        //TODO: Deprecated method to be change
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        let parallexHeaderHeight = CGFloat(324.0 * screen_height) / 812
        let parallexHeaderMinHeight = topBarHeight + 0.0 // stack view hieght
        self.headerView.frame = CGRect(x: 0, y: 0, width: screen_width, height: parallexHeaderHeight)
        self.headerView.translatesAutoresizingMaskIntoConstraints = false
        self.headerView.widthAnchor.constraint(equalToConstant: screen_width).isActive = true
        self.headerView.delegate = self
        self.headerView.isUserInteractionEnabled = true
        self.mainScrollView.delegate = self
        self.mainScrollView.parallaxHeader.minimumHeight = CGFloat(parallexHeaderMinHeight)
        self.mainScrollView.parallaxHeader.height = parallexHeaderHeight
        self.mainScrollView.parallaxHeader.mode = MXParallaxHeaderMode.fill
        self.mainScrollView.parallaxHeader.delegate = self
        self.headerView.delegate = self
        self.mainScrollView.parallaxHeader.view = self.headerView
        self.headerView.currentlyUsingAs = .otherUserProfile
    }
    
    private func scrollFrameSetup() {
        self.setTheFrameOfViews()
    }
    
    private func configureScrollView() {
        self.detailView.scrollView.contentSize = CGSize(width: screen_width * 5.0, height: 1.0)
    }
    
    private func HeaderDataSetUp() {
        switch self.userType {
        case .swaye:
            if let userData = self.userPortFolio {
                self.headerView.ConfigureHeaderDataForUserPortfolio(model: userData)
            }
        default:
            if let userData = self.artistDetail {
                self.headerView.ConfigureHeaderDataForArtistModel(model: userData)
            }
        }
    }
    
    
    ///SET THE FRAME OF VIEWS
    private func setTheFrameOfViews() {

      let tabBarHeight = self.tabBarController?.tabBar.frame.size.height ?? 70
      let vcHeight = screen_height-(45+40+tabBarHeight)
        
        self.statsVC.view.frame = CGRect(x: 0, y: 0, width: screen_width, height: vcHeight)
        self.newsVC.view.frame = CGRect(x: screen_width, y: 0, width: screen_width, height: vcHeight)
        self.musicVC.view.frame = CGRect(x: screen_width * 2, y: 0, width: screen_width, height: vcHeight)
        self.socialVC.view.frame = CGRect(x: screen_width * 3, y: 0, width: screen_width, height: vcHeight)
        self.eventsVC.view.frame = CGRect(x: screen_width * 4, y: 0, width: screen_width, height: vcHeight)

    }
    
    private func instantiateViewController() {

        self.detailView.scrollView.delegate = self
        self.mainScrollView.addSubview(self.detailView)
        
        let tabBarHeight = self.tabBarController?.tabBar.frame.size.height ?? 70
        let vcHeight = screen_height-(45+40+tabBarHeight)

        self.statsVC = StatsVC.instantiate(fromAppStoryboard: .Home)
        statsVC.userType = self.userType
        self.addChild(statsVC)
        self.statsVC.view.frame = CGRect(x: 0, y: 0, width: screen_width, height: vcHeight )
        self.detailView.scrollView.frame = self.statsVC.view.frame
        self.detailView.scrollView.addSubview(self.statsVC.view)
        self.statsVC.didMove(toParent: self)
        
        self.newsVC = NewsVC.instantiate(fromAppStoryboard: .Home)
        newsVC.artistId = (self.userType == .swaye ? self.userPortFolio?.artist.id : self.artistDetail?.id) ?? "1"
        self.addChild(self.newsVC)
        newsVC.view.frame = CGRect(x: screen_width, y: 0, width: screen_width, height: vcHeight )
        self.detailView.scrollView.frame = self.newsVC.view.frame
        self.detailView.scrollView.addSubview(self.newsVC.view)
        self.newsVC.didMove(toParent: self)
        
        self.musicVC = MusicVC.instantiate(fromAppStoryboard: .Home)
        self.addChild(self.musicVC)
        self.musicVC.view.frame = CGRect(x: 2 * screen_width, y: 0, width: screen_width, height: vcHeight )
        self.detailView.scrollView.frame = self.musicVC.view.frame
        self.detailView.scrollView.addSubview(self.musicVC.view)
        self.musicVC.didMove(toParent: self)
        
        self.socialVC = StatsSocialVC.instantiate(fromAppStoryboard: .Home)
        socialVC.artistId = (self.userType == .swaye ? self.userPortFolio?.artist.id : self.artistDetail?.id) ?? "1"
        self.addChild(self.socialVC)
        self.socialVC.view.frame = CGRect(x: 3 * screen_width, y: 0, width: screen_width, height: vcHeight )
        self.detailView.scrollView.frame = self.socialVC.view.frame
        self.detailView.scrollView.addSubview(self.socialVC.view)
        self.socialVC.didMove(toParent: self)
        
        self.eventsVC = EventsVC.instantiate(fromAppStoryboard: .Home)
        eventsVC.artistId = (self.userType == .swaye ? self.userPortFolio?.artist.id : self.artistDetail?.id) ?? "1"
        self.addChild(self.eventsVC)
        self.eventsVC.view.frame = CGRect(x: 4 * screen_width, y: 0, width: screen_width, height: vcHeight )
        self.detailView.scrollView.frame = self.eventsVC.view.frame
        self.detailView.scrollView.addSubview(self.eventsVC.view)
        self.eventsVC.didMove(toParent: self)
        
        
        self.detailView.scrollView.delegate = self
        
        self.configureScrollView()
        
    }
    
    
    private func buttonTapAction(senderTag: Int ) {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.1) {
            self.detailView.scrollView.setContentOffset(CGPoint(x: screen_width * CGFloat(senderTag), y: 0), animated: true)
            self.headerView.detailImgView.forEach { (btn) in
                if btn.tag == senderTag {
                    self.headerView.detailBtn[senderTag].alpha = 1.0
                    self.headerView.detailImgView[senderTag].alpha = 1.0
                } else {
                    self.headerView.detailBtn[btn.tag].alpha = 0.70
                    self.headerView.detailImgView[btn.tag].alpha = 0.0
                }
            }
            self.view.layoutIfNeeded()
        }
        
    }
}


extension SwayeOtherUserVC : MXParallaxHeaderDelegate ,MXScrollViewDelegate {
    
    
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        let prallexProgress = parallaxHeader.progress
        
        if isScrollingFirstTime && prallexProgress > 1.0 {
            maxValue = prallexProgress
            minValue = abs(1 - prallexProgress)
            finalMaxValue = Int(maxValue * 100)
            isScrollingFirstTime = false
        }
       
        if minValue...maxValue ~= prallexProgress {
            let intValue =  finalMaxValue - Int(prallexProgress * 100)
            
            let newProgress: Float = (Float(1) - (Float(1.3)  * (Float(intValue) / 100)))
            self.currentProgressIntValue = intValue
            self.currentProgress = newProgress.toCGFloat
        }
        //
        if prallexProgress  <= 0.15 {
            scrollToTop = true
            self.setNavigationBarClear = false
            self.addButtonOnRight(initial: false)
            self.animateBackView(isHidden: false)
        } else {
            self.setNavigationBarClear = true
            scrollToTop = false
            self.animateBackView(isHidden: true)
            self.addButtonOnRight()
        }
        
    }
    
    //MARK:- Public
    func animateBackView(isHidden: Bool) {
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
                           (self.navigationController?.navigationBar.frame.height ?? 0.0)
        let value = isHidden ? 0.0 : topBarHeight
        if self.headerView.bottomAnimationHC.constant != value {
            UIView.animate(withDuration: 0.35, animations: {
                self.headerView.bottomAnimationHC.constant = isHidden ? 0.0 : topBarHeight
                self.title =  isHidden ? "" : self.artistDetail?.name
                self.view.layoutIfNeeded()
            }) { (isDone) in
            }
        }
    }
    
    func scrollView(_ scrollView: MXScrollView, shouldScrollWithSubView subView: UIScrollView) -> Bool {
        return true
    }
}


//MARK:- OtherArtistHeaderViewDelegate
//===================================

extension SwayeOtherUserVC : OtherArtistHeaderViewDelegate {
    
    func detailBtnAction(sender: UIButton) {
        self.buttonTapAction(senderTag: sender.tag)
    }
    
    
}


//MARK:- Move the screen and pages accroding to scroll
//====================================================

extension SwayeOtherUserVC {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.detailView.scrollView {
            switch scrollView.contentOffset.x {
            case screen_width * 0:
                self.buttonTapAction(senderTag: 0)
            case screen_width * 1:
                self.buttonTapAction(senderTag: 1)
            case screen_width * 2:
                self.buttonTapAction(senderTag: 2)
            case screen_width * 3:
                self.buttonTapAction(senderTag: 3)
            case screen_width * 4:
                self.buttonTapAction(senderTag: 4)
            default:
                break
            }
        }
    }
        
}


//MARK:- UINavigationItem
//======================

extension SwayeOtherUserVC {
    
    
    func addButtonOnRight(initial: Bool = true){
        let shareBtn : UIButton = UIButton.init(type: .custom)
        let img = UIImage.init(named: "icShare")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        shareBtn.setImage(img, for: .normal)
        shareBtn.tintColor = initial ? .white : UIColor.AppColor.changeBlack
        shareBtn.addTarget(self, action: #selector(gotSharePage), for: .touchUpInside)
        shareBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let shareButton = UIBarButtonItem(customView: shareBtn)
        let searchBtn : UIButton = UIButton.init(type: .custom)
        let img1 = UIImage.init(named: "icSearch")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        searchBtn.setImage(img1, for: .normal)
        searchBtn.tintColor = initial ? .white : UIColor.AppColor.changeBlack
        searchBtn.addTarget(self, action: #selector(gotSearchPage), for: .touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let searchButton = UIBarButtonItem(customView: searchBtn)
        self.navigationItem.rightBarButtonItems = [searchButton,shareButton]
    }
    
    @objc func gotSharePage() {
        CommonFunctions.shareWithActivityViewController(VC: self)
    }
    
    @objc func gotSearchPage() {
       //TODO -
    }
}

//MARK: - API Calling
extension SwayeOtherUserVC {
    
    func getArtistDetail() {
        GraphQLServices.shared.getArtistDetail(artistId: (self.userType == .swaye ? self.userPortFolio?.artist.id : self.artistDetail?.id) ?? "1", success: { (json) in
            let artist = Artist(json[ApiKey.artist])
            self.artistDetail = artist
            self.headerView.ConfigureHeaderData(model: artist)
        }) { (error) -> (Void) in
            CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
    }
}



extension Int32 {
    var toCGFloat:CGFloat{
        return CGFloat(self)
    }
}
extension Float {
    
    var toCGFloat:CGFloat {
        return CGFloat(self)
    }
}
