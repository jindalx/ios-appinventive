//
//  NewsVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class NewsVC: UIViewController {
    
    //MARK:- IBOUTLETS
    //==================
    
    @IBOutlet weak var mainTableView: UITableView!
    
   
    var artistId: String = "1"
    
    private var news = [News]() {
        didSet {
            mainTableView.reloadData()
        }
    }
    
    
    //MARK:- VIEW LIFE CYCLE
    //==================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
    }
    //MARK:- PRIVATE FUNCTIONS
    //==================
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.footerViewSetUp()
        self.registerXib()
        self.getNews()
    }
    
    private func tableViewSetUp() {
        self.mainTableView.delegate    = self
        self.mainTableView.dataSource  = self
        self.mainTableView.isScrollEnabled = true
    }
    
    private func footerViewSetUp() {
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 181.0))
        self.mainTableView.tableFooterView = footerView
    }
    
    private func registerXib(){
        self.mainTableView.registerCell(with: NewsTableCell.self)
        self.mainTableView.registerHeaderFooter(with: NewsHeaderView.self)
        
    }
    
    
}
//MARK:- TABLEVIEW DELEGATE AND DATA SOURCE
//==================

extension NewsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.news.endIndex > 0 {
           return self.news.count
        } else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueCell(with: NewsTableCell.self)
        if self.news.endIndex > 0 {
            cell.populateData(model: self.news[indexPath.row])
        } else {}
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NewsDetailVC.instantiate(fromAppStoryboard: .Home)
        Router.shared.currentTabNavigation?.present(vc, animated: true, completion: nil)
         
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueHeaderFooter(with: NewsHeaderView.self)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (205 * screen_height) / 812
        
    }
    
    
}

//MARK: - API Calling
extension NewsVC {
    
    func getNews() {
        GraphQLServices.shared.getAllNews(artistId: self.artistId, pageNo: 1, pageSize: 10, loader: true, success: { (json) in
            let news = News.returnNewsListingArray(json[ApiKey.list])
            self.news = news
        }) { (error) -> (Void) in
            CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
    }
}
