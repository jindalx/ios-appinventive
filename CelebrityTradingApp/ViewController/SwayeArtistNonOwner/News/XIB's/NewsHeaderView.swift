//
//  NewsHeaderView.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class NewsHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var seeMoreStackView: UIStackView!
    @IBOutlet weak var headerTitle: UILabel!
    
}
