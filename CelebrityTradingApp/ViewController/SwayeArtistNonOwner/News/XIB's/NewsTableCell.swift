//
//  NewsTableCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class NewsTableCell: UITableViewCell {
    
    @IBOutlet weak var newsImgView: UIImageView!
    @IBOutlet weak var newsTitleLbl: UILabel!
    @IBOutlet weak var newsTimeLbl: UILabel!
    
    
    
    func populateData(model: News) {
        self.newsTitleLbl.text = model.__typename
        self.newsTimeLbl.text = model.publishedTime
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.newsImgView.layer.cornerRadius = 4.0
    }

    
    
}
