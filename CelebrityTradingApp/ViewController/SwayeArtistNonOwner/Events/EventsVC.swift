//
//  EventsVC.swift
//  CelebrityTradingApp
//
//  Created by Admin on 24/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class EventsVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!
    
     var artistId: String = "1"
    
    private var events = [Event]() {
        didSet {
            mainTableView.reloadData()
        }
    }
    
    //MARK:- VIEW LIFE CYCLE
    //==================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
        self.getEvents()
    }
    //MARK:- PRIVATE FUNCTIONS
    //==================
    
    private func initialSetUp() {
        self.tableViewSetUp()
        self.footerViewSetUp()
        self.registerXib()
    }
    
    private func tableViewSetUp() {
        self.mainTableView.delegate    = self
        self.mainTableView.dataSource  = self
        self.mainTableView.isScrollEnabled = true
    }
    
    private func footerViewSetUp() {
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 181.0))
        self.mainTableView.tableFooterView = footerView
    }
    
    private func registerXib(){
        self.mainTableView.registerCell(with: EventTableCell.self)
        self.mainTableView.registerHeaderFooter(with: NewsHeaderView.self)
        
    }
    
    
}
//MARK:- TABLEVIEW DELEGATE AND DATA SOURCE
//==================

extension EventsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.events.endIndex > 0 { return self.events.count
        } else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableCell", for: indexPath) as? EventTableCell else { fatalError("cell not found")}
        if self.events.endIndex > 0 {
        cell.populateData(model: self.events[indexPath.row])
        } else {
        }
        switch indexPath.row {
        case 0:
            return cell
        default:
            cell.topLineView.isHidden = true
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "NewsHeaderView") as? NewsHeaderView
        view?.headerTitle.text = "Events"
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
}

//MARK: - API Calling
extension EventsVC {
    
    func getEvents() {
        GraphQLServices.shared.getEvents(artistId: self.artistId, pageNo: 1, pageSize: 10, success: { (json) in
            
            let eventsDetail = Event.returnEventsListingArray(json[ApiKey.list])
            self.events = eventsDetail
        }) { (error) -> (Void) in
            CommonFunctions.showToastWithMessage(msg: error.localizedDescription)
        }
    }
}


