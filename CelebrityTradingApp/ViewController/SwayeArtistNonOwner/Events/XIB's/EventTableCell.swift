//
//  EventTableCell.swift
//  CelebrityTradingApp
//
//  Created by Admin on 24/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class EventTableCell: UITableViewCell {
    
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var dayDateLbl: UILabel!
    @IBOutlet weak var placeLbl: UILabel!
    

    
    func populateData(model: Event) {
        self.dayDateLbl.text = model.date.getFormattedDate()
        self.placeLbl.text = model.location
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}
