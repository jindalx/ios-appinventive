//
//  OtherArtistHeaderView.swift
//  CelebrityTradingApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//


import UIKit
import Foundation

protocol OtherArtistHeaderViewDelegate: NSObjectProtocol {
    func detailBtnAction(sender: UIButton)
}

class OtherArtistHeaderView: UIView {
    
    
    enum typeOfView {
        case otherUserProfile
    }
    
    weak var delegate: OtherArtistHeaderViewDelegate?
    
    var mainIMgContainerView : UIView?
    
    
    
    //MARK:- IBOUTLETS
    //==================
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var mainImgBackView: GradientView!
    @IBOutlet weak var watchBtn: UIButton!
    @IBOutlet weak var watchIconView: UIView!
    @IBOutlet weak var stockPriceView: UIView!
    @IBOutlet var detailBtn: [UIButton]!
    @IBOutlet var detailImgView: [UIButton]!
    @IBOutlet weak var mainImgView: UIImageView!
    @IBOutlet weak var bottomAnimationView: UIView!
    @IBOutlet weak var bottomAnimationHC: NSLayoutConstraint!
    
    
    var currentlyUsingAs = typeOfView.otherUserProfile {
        didSet {
            switch currentlyUsingAs {
            case .otherUserProfile:
                self.setupForotherUserProfile()
            }
        }
    }
    
    
    override func setNeedsLayout() {
        super.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.stockPriceView.roundCorners([.topRight,.bottomRight], radius: 12.5)
    }
    
    //MARK:- VIEW LIFE CYCLE
    //=====================
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetUp()
        
    }
    
    
    
    //MARK:- PRIVATE FUNCTIONS
    //=======================
    
    private func initialSetUp() {
        self.watchIconView.applyGradient(colors: [UIColor.init(r: 34, g: 131, b: 184, alpha: 1.0).cgColor,UIColor.init(r: 15, g: 76, b: 130, alpha: 1.0).cgColor])
        self.scrollButtonSetUp()
        
    }
    
    
    private func scrollButtonSetUp() {
        detailImgView.forEach { (button) in
            detailImgView[button.tag].alpha = 0.0
            detailBtn[button.tag].alpha = 0.70
        }
        detailImgView[0].alpha = 1.0
        detailBtn[0].alpha = 1.0
    }
    
    private func addGradientOnView() {
//        self.mainImgBackView.layer.masksToBounds = true
//        self.mainImgBackView.addGradient(colors: UIColor.AppColor.otherUserProfileGradientColors)
    }
    
    
    private func setupForotherUserProfile() {
       self.addGradientOnView()
    }
    
    
    public func ConfigureHeaderData(model: Artist) {
        self.mainImgView.setImage(withUrl: model.img, placeholderImage: nil)
        self.profileNameLbl.text = model.name
        
    }
    
    public func ConfigureHeaderDataForUserPortfolio(model: UserPortofolio){
        self.profileNameLbl.text = model.artist.name
        self.watchIconView.isHidden = true
        self.stockPriceView.backgroundColor = UIColor.init(r: 255, g: 0, b: 68, alpha: 0.6)
    }
    
    public func ConfigureHeaderDataForArtistModel(model: Artist){
        self.profileNameLbl.text = model.name
    }
 
    //MARK:- IBACTIONS
    //==================
    @IBAction func detailBtnAction(_ sender: UIButton) {
        self.delegate?.detailBtnAction(sender: sender)
    }
    
    @IBAction func watchBtnActions(_ sender: UIButton) {
        self.watchBtn.setImage( sender.isSelected ?  #imageLiteral(resourceName: "icWatch") : #imageLiteral(resourceName: "icUnwatch") , for: .normal)
       
        if sender.isSelected {   self.watchIconView.applyGradient(colors: [UIColor.init(r: 34, g: 131, b: 184, alpha: 1.0).cgColor,UIColor.init(r: 15, g: 76, b: 130, alpha: 1.0).cgColor])
              self.stockPriceView.backgroundColor = UIColor.init(r: 89, g: 161, b: 7, alpha: 0.6)
            } else { if let layer = watchIconView.layer.sublayers?.first {
                layer.removeFromSuperlayer ()
            self.watchIconView.backgroundColor = .white
            self.stockPriceView.backgroundColor = UIColor.init(r: 255, g: 0, b: 68, alpha: 0.6)
            }
        }
        self.watchBtn.isSelected =  !sender.isSelected
    }
    
    
    class func instanciateFromNib() -> OtherArtistHeaderView {
        return Bundle .main .loadNibNamed("OtherArtistHeaderView", owner: self, options: nil)![0] as! OtherArtistHeaderView
    }
    
    
    
}
