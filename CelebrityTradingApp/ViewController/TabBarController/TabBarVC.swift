//
//  TabBarVC.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doNavAndTabCustomization()
        self.tabSetup()
        self.addAudioPlaybackView()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
    func tabSetup() {
        let homeTab =   self.tabBar.items?[0]
        let forYouTab =   self.tabBar.items?[1]
        let artistTab =   self.tabBar.items?[2]
        let alertTab =   self.tabBar.items?[3]
        let accountTab =   self.tabBar.items?[4]
        
        let tabs = [homeTab,forYouTab,artistTab,alertTab,accountTab]
        let tabsTittle = [LocalString.homeTitle, LocalString.forYouTitle, LocalString.artistTitle, LocalString.alertTitle, LocalString.accountTitle]
        
        for eachTab in tabs {
            guard let indexOfeachTab = tabs.firstIndex(of: eachTab) else { return }
            self.setTabBarItem(eachTab ?? tabBarItem, tabsTittle[indexOfeachTab], indexOfeachTab)
        }
        
        //For normal state, the color is clear color, so you will not see any title until your tab is selected.
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for:.normal)
        //Set any color for selected state
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: self.tabBar.tintColor ?? UIColor.blue], for:.selected)
        self.tabBarController?.tabBar.backgroundColor = .clear
        self.tabBarController?.tabBar.backgroundImage =  #imageLiteral(resourceName: "clear")
    }
    
    private func doNavAndTabCustomization(){
        
      //  UINavigationBar.appearance().barTintColor = UIColor.AppColor.navigationColor
        
        /*//Uncomment it if required
        self.tabBar.barTintColor = .white
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().layer.borderColor = UIColor.clear.cgColor
        
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().clipsToBounds = true
        UINavigationBar.appearance().layer.borderColor = UIColor.clear.cgColor */
        
       // UINavigationBar.appearance().tintColor = UIColor.AppColor.changeBlack
    }
    
    func setTabBarItem(_ tabBarItem: UITabBarItem, _ title: String, _ tag: Int) -> Void {
        tabBarItem.title = title
        tabBarItem.tag = tag
    }
    
    //MARK: Music Bar Setup
    public func addAudioPlaybackView() {
        let containerView = MusicTabBarView()
        view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        // anchor your view right above the tabBar
        containerView.bottomAnchor.constraint(equalTo: tabBar.topAnchor).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: tabBar.frame.height+18).isActive = true
       // containerView.contentView.backgroundColor =
           // (self.tabBar.barTintColor != nil)
            //?
           //     self.tabBar.barTintColor
          //  :
          //  UIColor.AppColor.backgroundGrey3
        containerView.backgroundColor = .clear
    }
}

/* Delegate Methods */
extension TabBarVC {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let tabItemIndex = tabBar.items?.firstIndex(of: item) else { return }
        printDebug("Selected tab -> \(tabItemIndex)")
    }
}


class FrostyTabBar: UITabBar {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let frost = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        frost.frame = bounds
        frost.autoresizingMask = .flexibleWidth
        insertSubview(frost, at: 0)
    }
}
