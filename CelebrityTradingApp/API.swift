// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public final class ArtistDetailQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ArtistDetail($artistId: ID) {
      artist(artistId: $artistId) {
        __typename
        id
        name
        symbol
        img
        tradeStats {
          __typename
          sparkline {
            __typename
            date
            value
          }
        }
        tradeSettings {
          __typename
          buyEnabled
          sellEnabled
        }
        recentNews {
          __typename
          id
          artist {
            __typename
            name
          }
        }
      }
    }
    """

  public let operationName: String = "ArtistDetail"

  public var artistId: GraphQLID?

  public init(artistId: GraphQLID? = nil) {
    self.artistId = artistId
  }

  public var variables: GraphQLMap? {
    return ["artistId": artistId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MyArtistQuery"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("artist", arguments: ["artistId": GraphQLVariable("artistId")], type: .object(Artist.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(artist: Artist? = nil) {
      self.init(unsafeResultMap: ["__typename": "MyArtistQuery", "artist": artist.flatMap { (value: Artist) -> ResultMap in value.resultMap }])
    }

    public var artist: Artist? {
      get {
        return (resultMap["artist"] as? ResultMap).flatMap { Artist(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "artist")
      }
    }

    public struct Artist: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PerformerGraphType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("symbol", type: .nonNull(.scalar(String.self))),
        GraphQLField("img", type: .nonNull(.scalar(String.self))),
        GraphQLField("tradeStats", type: .object(TradeStat.selections)),
        GraphQLField("tradeSettings", type: .object(TradeSetting.selections)),
        GraphQLField("recentNews", type: .object(RecentNews.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: String, name: String, symbol: String, img: String, tradeStats: TradeStat? = nil, tradeSettings: TradeSetting? = nil, recentNews: RecentNews? = nil) {
        self.init(unsafeResultMap: ["__typename": "PerformerGraphType", "id": id, "name": name, "symbol": symbol, "img": img, "tradeStats": tradeStats.flatMap { (value: TradeStat) -> ResultMap in value.resultMap }, "tradeSettings": tradeSettings.flatMap { (value: TradeSetting) -> ResultMap in value.resultMap }, "recentNews": recentNews.flatMap { (value: RecentNews) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return resultMap["id"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var symbol: String {
        get {
          return resultMap["symbol"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "symbol")
        }
      }

      public var img: String {
        get {
          return resultMap["img"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "img")
        }
      }

      public var tradeStats: TradeStat? {
        get {
          return (resultMap["tradeStats"] as? ResultMap).flatMap { TradeStat(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "tradeStats")
        }
      }

      public var tradeSettings: TradeSetting? {
        get {
          return (resultMap["tradeSettings"] as? ResultMap).flatMap { TradeSetting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "tradeSettings")
        }
      }

      public var recentNews: RecentNews? {
        get {
          return (resultMap["recentNews"] as? ResultMap).flatMap { RecentNews(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "recentNews")
        }
      }

      public struct TradeStat: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["TradeStatsGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("sparkline", type: .list(.object(Sparkline.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(sparkline: [Sparkline?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "TradeStatsGraphType", "sparkline": sparkline.flatMap { (value: [Sparkline?]) -> [ResultMap?] in value.map { (value: Sparkline?) -> ResultMap? in value.flatMap { (value: Sparkline) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var sparkline: [Sparkline?]? {
          get {
            return (resultMap["sparkline"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Sparkline?] in value.map { (value: ResultMap?) -> Sparkline? in value.flatMap { (value: ResultMap) -> Sparkline in Sparkline(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Sparkline?]) -> [ResultMap?] in value.map { (value: Sparkline?) -> ResultMap? in value.flatMap { (value: Sparkline) -> ResultMap in value.resultMap } } }, forKey: "sparkline")
          }
        }

        public struct Sparkline: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["TimeChartDataGraphType"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("date", type: .nonNull(.scalar(String.self))),
            GraphQLField("value", type: .nonNull(.scalar(Double.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(date: String, value: Double) {
            self.init(unsafeResultMap: ["__typename": "TimeChartDataGraphType", "date": date, "value": value])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var date: String {
            get {
              return resultMap["date"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "date")
            }
          }

          public var value: Double {
            get {
              return resultMap["value"]! as! Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "value")
            }
          }
        }
      }

      public struct TradeSetting: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["TradeSettingGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("buyEnabled", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("sellEnabled", type: .nonNull(.scalar(Bool.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(buyEnabled: Bool, sellEnabled: Bool) {
          self.init(unsafeResultMap: ["__typename": "TradeSettingGraphType", "buyEnabled": buyEnabled, "sellEnabled": sellEnabled])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var buyEnabled: Bool {
          get {
            return resultMap["buyEnabled"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "buyEnabled")
          }
        }

        public var sellEnabled: Bool {
          get {
            return resultMap["sellEnabled"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "sellEnabled")
          }
        }
      }

      public struct RecentNews: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["NewsItemGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("artist", type: .object(Artist.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, artist: Artist? = nil) {
          self.init(unsafeResultMap: ["__typename": "NewsItemGraphType", "id": id, "artist": artist.flatMap { (value: Artist) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var artist: Artist? {
          get {
            return (resultMap["artist"] as? ResultMap).flatMap { Artist(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "artist")
          }
        }

        public struct Artist: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PerformerGraphType"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("name", type: .nonNull(.scalar(String.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(name: String) {
            self.init(unsafeResultMap: ["__typename": "PerformerGraphType", "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}

public final class ArtistNewsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query ArtistNews($artistId: ID, $pageNo: Int!, $pageSize: Int!) {
      news(artistId: $artistId, pageNo: $pageNo, pageSize: $pageSize) {
        __typename
        total
        list {
          __typename
          id
          publishedTime
          artist {
            __typename
            id
            name
          }
        }
      }
    }
    """

  public let operationName: String = "ArtistNews"

  public var artistId: GraphQLID?
  public var pageNo: Int
  public var pageSize: Int

  public init(artistId: GraphQLID? = nil, pageNo: Int, pageSize: Int) {
    self.artistId = artistId
    self.pageNo = pageNo
    self.pageSize = pageSize
  }

  public var variables: GraphQLMap? {
    return ["artistId": artistId, "pageNo": pageNo, "pageSize": pageSize]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MyArtistQuery"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("news", arguments: ["artistId": GraphQLVariable("artistId"), "pageNo": GraphQLVariable("pageNo"), "pageSize": GraphQLVariable("pageSize")], type: .object(News.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(news: News? = nil) {
      self.init(unsafeResultMap: ["__typename": "MyArtistQuery", "news": news.flatMap { (value: News) -> ResultMap in value.resultMap }])
    }

    public var news: News? {
      get {
        return (resultMap["news"] as? ResultMap).flatMap { News(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "news")
      }
    }

    public struct News: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["NewsResponseGraphType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("total", type: .nonNull(.scalar(Int.self))),
        GraphQLField("list", type: .list(.object(List.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(total: Int, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "NewsResponseGraphType", "total": total, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var total: Int {
        get {
          return resultMap["total"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "total")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["NewsItemGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("publishedTime", type: .nonNull(.scalar(String.self))),
          GraphQLField("artist", type: .object(Artist.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, publishedTime: String, artist: Artist? = nil) {
          self.init(unsafeResultMap: ["__typename": "NewsItemGraphType", "id": id, "publishedTime": publishedTime, "artist": artist.flatMap { (value: Artist) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var publishedTime: String {
          get {
            return resultMap["publishedTime"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "publishedTime")
          }
        }

        public var artist: Artist? {
          get {
            return (resultMap["artist"] as? ResultMap).flatMap { Artist(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "artist")
          }
        }

        public struct Artist: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PerformerGraphType"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(String.self))),
            GraphQLField("name", type: .nonNull(.scalar(String.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String, name: String) {
            self.init(unsafeResultMap: ["__typename": "PerformerGraphType", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String {
            get {
              return resultMap["id"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}

public final class UserWatchlistQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query UserWatchlist($pageNo: Int!, $pageSize: Int!) {
      watchlist(pageNo: $pageNo, pageSize: $pageSize) {
        __typename
        total
        artists {
          __typename
          id
          img
          name
          symbol
          recentNews {
            __typename
            id
          }
        }
      }
    }
    """

  public let operationName: String = "UserWatchlist"

  public var pageNo: Int
  public var pageSize: Int

  public init(pageNo: Int, pageSize: Int) {
    self.pageNo = pageNo
    self.pageSize = pageSize
  }

  public var variables: GraphQLMap? {
    return ["pageNo": pageNo, "pageSize": pageSize]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MyArtistQuery"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("watchlist", arguments: ["pageNo": GraphQLVariable("pageNo"), "pageSize": GraphQLVariable("pageSize")], type: .object(Watchlist.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(watchlist: Watchlist? = nil) {
      self.init(unsafeResultMap: ["__typename": "MyArtistQuery", "watchlist": watchlist.flatMap { (value: Watchlist) -> ResultMap in value.resultMap }])
    }

    public var watchlist: Watchlist? {
      get {
        return (resultMap["watchlist"] as? ResultMap).flatMap { Watchlist(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "watchlist")
      }
    }

    public struct Watchlist: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ArtistsResponseGraphType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("total", type: .nonNull(.scalar(Int.self))),
        GraphQLField("artists", type: .list(.object(Artist.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(total: Int, artists: [Artist?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "ArtistsResponseGraphType", "total": total, "artists": artists.flatMap { (value: [Artist?]) -> [ResultMap?] in value.map { (value: Artist?) -> ResultMap? in value.flatMap { (value: Artist) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var total: Int {
        get {
          return resultMap["total"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "total")
        }
      }

      public var artists: [Artist?]? {
        get {
          return (resultMap["artists"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Artist?] in value.map { (value: ResultMap?) -> Artist? in value.flatMap { (value: ResultMap) -> Artist in Artist(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Artist?]) -> [ResultMap?] in value.map { (value: Artist?) -> ResultMap? in value.flatMap { (value: Artist) -> ResultMap in value.resultMap } } }, forKey: "artists")
        }
      }

      public struct Artist: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PerformerGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("img", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("symbol", type: .nonNull(.scalar(String.self))),
          GraphQLField("recentNews", type: .object(RecentNews.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, img: String, name: String, symbol: String, recentNews: RecentNews? = nil) {
          self.init(unsafeResultMap: ["__typename": "PerformerGraphType", "id": id, "img": img, "name": name, "symbol": symbol, "recentNews": recentNews.flatMap { (value: RecentNews) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var img: String {
          get {
            return resultMap["img"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "img")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var symbol: String {
          get {
            return resultMap["symbol"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "symbol")
          }
        }

        public var recentNews: RecentNews? {
          get {
            return (resultMap["recentNews"] as? ResultMap).flatMap { RecentNews(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "recentNews")
          }
        }

        public struct RecentNews: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["NewsItemGraphType"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(String.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String) {
            self.init(unsafeResultMap: ["__typename": "NewsItemGraphType", "id": id])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String {
            get {
              return resultMap["id"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }
        }
      }
    }
  }
}

public final class UserBalanceQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query UserBalance {
      balance {
        __typename
        currency
        balance
      }
    }
    """

  public let operationName: String = "UserBalance"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MyArtistQuery"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("balance", type: .list(.object(Balance.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(balance: [Balance?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "MyArtistQuery", "balance": balance.flatMap { (value: [Balance?]) -> [ResultMap?] in value.map { (value: Balance?) -> ResultMap? in value.flatMap { (value: Balance) -> ResultMap in value.resultMap } } }])
    }

    public var balance: [Balance?]? {
      get {
        return (resultMap["balance"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Balance?] in value.map { (value: ResultMap?) -> Balance? in value.flatMap { (value: ResultMap) -> Balance in Balance(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [Balance?]) -> [ResultMap?] in value.map { (value: Balance?) -> ResultMap? in value.flatMap { (value: Balance) -> ResultMap in value.resultMap } } }, forKey: "balance")
      }
    }

    public struct Balance: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["BalanceDataGraphType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("currency", type: .nonNull(.scalar(String.self))),
        GraphQLField("balance", type: .nonNull(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(currency: String, balance: String) {
        self.init(unsafeResultMap: ["__typename": "BalanceDataGraphType", "currency": currency, "balance": balance])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var currency: String {
        get {
          return resultMap["currency"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "currency")
        }
      }

      public var balance: String {
        get {
          return resultMap["balance"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "balance")
        }
      }
    }
  }
}

public final class UserPortofolioQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query UserPortofolio($pageNo: Int!, $pageSize: Int!) {
      portfolio(pageNo: $pageNo, pageSize: $pageSize) {
        __typename
        total
        artists {
          __typename
          balance
          value
          artist {
            __typename
            id
            name
            img
            symbol
          }
        }
      }
    }
    """

  public let operationName: String = "UserPortofolio"

  public var pageNo: Int
  public var pageSize: Int

  public init(pageNo: Int, pageSize: Int) {
    self.pageNo = pageNo
    self.pageSize = pageSize
  }

  public var variables: GraphQLMap? {
    return ["pageNo": pageNo, "pageSize": pageSize]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MyArtistQuery"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("portfolio", arguments: ["pageNo": GraphQLVariable("pageNo"), "pageSize": GraphQLVariable("pageSize")], type: .object(Portfolio.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(portfolio: Portfolio? = nil) {
      self.init(unsafeResultMap: ["__typename": "MyArtistQuery", "portfolio": portfolio.flatMap { (value: Portfolio) -> ResultMap in value.resultMap }])
    }

    public var portfolio: Portfolio? {
      get {
        return (resultMap["portfolio"] as? ResultMap).flatMap { Portfolio(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "portfolio")
      }
    }

    public struct Portfolio: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PortfolioResponseGraphType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("total", type: .nonNull(.scalar(Int.self))),
        GraphQLField("artists", type: .list(.object(Artist.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(total: Int, artists: [Artist?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "PortfolioResponseGraphType", "total": total, "artists": artists.flatMap { (value: [Artist?]) -> [ResultMap?] in value.map { (value: Artist?) -> ResultMap? in value.flatMap { (value: Artist) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var total: Int {
        get {
          return resultMap["total"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "total")
        }
      }

      public var artists: [Artist?]? {
        get {
          return (resultMap["artists"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Artist?] in value.map { (value: ResultMap?) -> Artist? in value.flatMap { (value: ResultMap) -> Artist in Artist(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Artist?]) -> [ResultMap?] in value.map { (value: Artist?) -> ResultMap? in value.flatMap { (value: Artist) -> ResultMap in value.resultMap } } }, forKey: "artists")
        }
      }

      public struct Artist: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PortfolioItemGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("balance", type: .nonNull(.scalar(Double.self))),
          GraphQLField("value", type: .nonNull(.scalar(Double.self))),
          GraphQLField("artist", type: .object(Artist.selections)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(balance: Double, value: Double, artist: Artist? = nil) {
          self.init(unsafeResultMap: ["__typename": "PortfolioItemGraphType", "balance": balance, "value": value, "artist": artist.flatMap { (value: Artist) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var balance: Double {
          get {
            return resultMap["balance"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "balance")
          }
        }

        public var value: Double {
          get {
            return resultMap["value"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "value")
          }
        }

        public var artist: Artist? {
          get {
            return (resultMap["artist"] as? ResultMap).flatMap { Artist(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "artist")
          }
        }

        public struct Artist: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PerformerGraphType"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(String.self))),
            GraphQLField("name", type: .nonNull(.scalar(String.self))),
            GraphQLField("img", type: .nonNull(.scalar(String.self))),
            GraphQLField("symbol", type: .nonNull(.scalar(String.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String, name: String, img: String, symbol: String) {
            self.init(unsafeResultMap: ["__typename": "PerformerGraphType", "id": id, "name": name, "img": img, "symbol": symbol])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String {
            get {
              return resultMap["id"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var img: String {
            get {
              return resultMap["img"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "img")
            }
          }

          public var symbol: String {
            get {
              return resultMap["symbol"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "symbol")
            }
          }
        }
      }
    }
  }
}

public final class EventsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query Events($artistId: ID, $pageNo: Int!, $pageSize: Int!) {
      events(artistId: $artistId, pageNo: $pageNo, pageSize: $pageSize) {
        __typename
        total
        list {
          __typename
          id
          name
          date
          location
          url
        }
      }
    }
    """

  public let operationName: String = "Events"

  public var artistId: GraphQLID?
  public var pageNo: Int
  public var pageSize: Int

  public init(artistId: GraphQLID? = nil, pageNo: Int, pageSize: Int) {
    self.artistId = artistId
    self.pageNo = pageNo
    self.pageSize = pageSize
  }

  public var variables: GraphQLMap? {
    return ["artistId": artistId, "pageNo": pageNo, "pageSize": pageSize]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MyArtistQuery"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("events", arguments: ["artistId": GraphQLVariable("artistId"), "pageNo": GraphQLVariable("pageNo"), "pageSize": GraphQLVariable("pageSize")], type: .object(Event.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(events: Event? = nil) {
      self.init(unsafeResultMap: ["__typename": "MyArtistQuery", "events": events.flatMap { (value: Event) -> ResultMap in value.resultMap }])
    }

    public var events: Event? {
      get {
        return (resultMap["events"] as? ResultMap).flatMap { Event(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "events")
      }
    }

    public struct Event: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["EventsResponseGraphType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("total", type: .nonNull(.scalar(Int.self))),
        GraphQLField("list", type: .list(.object(List.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(total: Int, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "EventsResponseGraphType", "total": total, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var total: Int {
        get {
          return resultMap["total"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "total")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["EventItemGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("date", type: .nonNull(.scalar(String.self))),
          GraphQLField("location", type: .nonNull(.scalar(String.self))),
          GraphQLField("url", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, name: String, date: String, location: String, url: String) {
          self.init(unsafeResultMap: ["__typename": "EventItemGraphType", "id": id, "name": name, "date": date, "location": location, "url": url])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var date: String {
          get {
            return resultMap["date"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "date")
          }
        }

        public var location: String {
          get {
            return resultMap["location"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "location")
          }
        }

        public var url: String {
          get {
            return resultMap["url"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "url")
          }
        }
      }
    }
  }
}

public final class SocialMediaQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query SocialMedia($artistId: ID, $pageNo: Int!, $pageSize: Int!) {
      socialMedia(artistId: $artistId, pageNo: $pageNo, pageSize: $pageSize) {
        __typename
        total
        list {
          __typename
          id
          artist {
            __typename
            id
            name
          }
          publishedTime
          content
          thumbnail
          postUrl
        }
      }
    }
    """

  public let operationName: String = "SocialMedia"

  public var artistId: GraphQLID?
  public var pageNo: Int
  public var pageSize: Int

  public init(artistId: GraphQLID? = nil, pageNo: Int, pageSize: Int) {
    self.artistId = artistId
    self.pageNo = pageNo
    self.pageSize = pageSize
  }

  public var variables: GraphQLMap? {
    return ["artistId": artistId, "pageNo": pageNo, "pageSize": pageSize]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MyArtistQuery"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("socialMedia", arguments: ["artistId": GraphQLVariable("artistId"), "pageNo": GraphQLVariable("pageNo"), "pageSize": GraphQLVariable("pageSize")], type: .object(SocialMedium.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(socialMedia: SocialMedium? = nil) {
      self.init(unsafeResultMap: ["__typename": "MyArtistQuery", "socialMedia": socialMedia.flatMap { (value: SocialMedium) -> ResultMap in value.resultMap }])
    }

    public var socialMedia: SocialMedium? {
      get {
        return (resultMap["socialMedia"] as? ResultMap).flatMap { SocialMedium(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "socialMedia")
      }
    }

    public struct SocialMedium: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SocialMediaResponseGraphType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("total", type: .nonNull(.scalar(Int.self))),
        GraphQLField("list", type: .list(.object(List.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(total: Int, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "SocialMediaResponseGraphType", "total": total, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var total: Int {
        get {
          return resultMap["total"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "total")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["SocialMediaItemGraphType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(String.self))),
          GraphQLField("artist", type: .object(Artist.selections)),
          GraphQLField("publishedTime", type: .nonNull(.scalar(String.self))),
          GraphQLField("content", type: .nonNull(.scalar(String.self))),
          GraphQLField("thumbnail", type: .nonNull(.scalar(String.self))),
          GraphQLField("postUrl", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: String, artist: Artist? = nil, publishedTime: String, content: String, thumbnail: String, postUrl: String) {
          self.init(unsafeResultMap: ["__typename": "SocialMediaItemGraphType", "id": id, "artist": artist.flatMap { (value: Artist) -> ResultMap in value.resultMap }, "publishedTime": publishedTime, "content": content, "thumbnail": thumbnail, "postUrl": postUrl])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var artist: Artist? {
          get {
            return (resultMap["artist"] as? ResultMap).flatMap { Artist(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "artist")
          }
        }

        public var publishedTime: String {
          get {
            return resultMap["publishedTime"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "publishedTime")
          }
        }

        public var content: String {
          get {
            return resultMap["content"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "content")
          }
        }

        public var thumbnail: String {
          get {
            return resultMap["thumbnail"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "thumbnail")
          }
        }

        public var postUrl: String {
          get {
            return resultMap["postUrl"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postUrl")
          }
        }

        public struct Artist: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PerformerGraphType"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(String.self))),
            GraphQLField("name", type: .nonNull(.scalar(String.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: String, name: String) {
            self.init(unsafeResultMap: ["__typename": "PerformerGraphType", "id": id, "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: String {
            get {
              return resultMap["id"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}
