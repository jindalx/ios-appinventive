//
//  Network.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 23/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import Foundation
import Apollo

// MARK: - Singleton Wrapper

class Network {

  static let auth = AppUserDefaults.value(forKey: .authorization).stringValue
    
  static let shared = Network()
  
 static let endPoint = "https://uoex-api.swayeformusic.com/graphql"
    
  // Configure the network transport to use the singleton as the delegate.
  private lazy var networkTransport: HTTPNetworkTransport = {
    let transport = HTTPNetworkTransport(url: URL(string: Network.endPoint)!)
    transport.delegate = self
    return transport
  }()
    
  // Use the configured network transport in your Apollo client.
  private(set) lazy var apollo = ApolloClient(networkTransport: self.networkTransport)
}

// MARK: - Pre-flight delegate

extension Network: HTTPNetworkTransportPreflightDelegate {

  func networkTransport(_ networkTransport: HTTPNetworkTransport,
                          shouldSend request: URLRequest) -> Bool {
    // If there's an authenticated user, send the request. If not, don't.
   // return UserManager.shared.hasAuthenticatedUser
    return true
  }
  
  func networkTransport(_ networkTransport: HTTPNetworkTransport,
                        willSend request: inout URLRequest) {
                        
    // Get the existing headers, or create new ones if they're nil
    var headers = request.allHTTPHeaderFields ?? [String: String]()

    // Add any new headers you need
    headers["Authorization"] = "Bearer \(Network.auth)"
  
    request.allHTTPHeaderFields = headers
    debugPrint( "Outgoing request: \(request)")
  }
}

// MARK: - Task Completed Delegate

extension Network: HTTPNetworkTransportTaskCompletedDelegate {
  func networkTransport(_ networkTransport: HTTPNetworkTransport,
                        didCompleteRawTaskForRequest request: URLRequest,
                        withData data: Data?,
                        response: URLResponse?,
                        error: Error?) {
    debugPrint("Raw task completed for request: \(request)")

   // Logger.log(.debug, "Raw task completed for request: \(request)")
                        
    if let error = error {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "noInternetConnection"), object: nil)
         debugPrint("Error: \(error)")
    }
    
    if let response = response {
         debugPrint("Response: \(response)")
    } else {
         debugPrint("No URL Response received!")
    }
    
    if let data = data {
        debugPrint("Data: \(String(describing: String(bytes: data, encoding: .utf8)))")
    } else {
      debugPrint("no data recieved")
    }
  }
    
    func dataToJSON(data: Data) -> Any? {
       do {
           return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
       } catch let myJSONError {
           print(myJSONError)
       }
       return nil
    }
    
}

// MARK: - Retry Delegate

extension Network: HTTPNetworkTransportRetryDelegate {
    
    func networkTransport(_ networkTransport: HTTPNetworkTransport, receivedError error: Error, for request: URLRequest, response: URLResponse?, continueHandler: @escaping (HTTPNetworkTransport.ContinueAction) -> Void) {
        
    }
    
    
    func networkTransport(_ networkTransport: HTTPNetworkTransport, receivedError error: Error, for request: URLRequest, response: URLResponse?, retryHandler: @escaping (Bool) -> Void) {
        print(response.jsonValue)
    }
    
}

