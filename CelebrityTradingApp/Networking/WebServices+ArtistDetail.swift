//
//  WebServices+ArtistDetail.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 30/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

extension GraphQLServices {
    
    //get artist detail
    func getArtistDetail(artistId: String, loader: Bool = false,success: @escaping SuccessJSONResponse, failure: @escaping FailureResponse) {
        
        let artistsQuery = ArtistDetailQuery(artistId: artistId)
        Network.shared.apollo.fetch(query: artistsQuery) { result in
            switch result {
            case .success(let graphQLResult):
                success(JSON(graphQLResult.data?.jsonObject as Any))
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    func getEvents(artistId: String, pageNo: Int, pageSize: Int, loader: Bool = false,success: @escaping SuccessJSONResponse, failure: @escaping FailureResponse) {
        
        let eventsQuery = EventsQuery(artistId: artistId, pageNo: pageNo, pageSize: pageSize)
        Network.shared.apollo.fetch(query: eventsQuery) { result in
            switch result {
            case .success(let graphQLResult):
                success(JSON(graphQLResult.data?.events?.jsonObject as Any))
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    func getSocialMedia(artistId: String, pageNo: Int = 1, pageSize: Int = 10, loader: Bool = false,success: @escaping SuccessJSONResponse, failure: @escaping FailureResponse) {
        
        let socialMediaQuery = SocialMediaQuery(artistId: artistId, pageNo: pageNo, pageSize: pageSize)
        Network.shared.apollo.fetch(query: socialMediaQuery) { result in
            switch result {
            case .success(let graphQLResult):
                success(JSON(graphQLResult.data?.socialMedia?.jsonObject as Any))
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    //Get News API
    func getAllNews(artistId: String, pageNo: Int = 1, pageSize: Int = 10, loader: Bool = false,success: @escaping SuccessJSONResponse, failure: @escaping FailureResponse) {
        
        let artistNews = ArtistNewsQuery(artistId: artistId, pageNo: pageNo, pageSize: pageSize)
         
         Network.shared.apollo.fetch(query: artistNews) { result in
             switch result {
             case .success(let graphQLResult):
                success(JSON(graphQLResult.data?.news?.jsonObject as Any))
             case .failure(let error):
                failure(error)
             }
         }
     }
    
}
