//
//  WebServices+HomeAPI.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 30/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON

typealias JSONDictionary = [String : Any]
typealias JSONDictionaryArray = [JSONDictionary]
typealias SuccessJSONResponse = (_ json : JSON) -> Void
typealias FailureResponse = (Error) -> (Void)
typealias ResponseMessage = (_ message : String) -> ()

class GraphQLServices {
       
    static let shared = GraphQLServices()
    
    private init() {
        //Singelton class
    }
    
    func getAllArtists(pageNo: Int = 1, pageSize: Int = 5, loader: Bool = false,success: @escaping SuccessJSONResponse, failure: @escaping FailureResponse) {
        
        let artistsQuery = UserPortofolioQuery(pageNo: pageNo, pageSize: pageSize)
        Network.shared.apollo.fetch(query: artistsQuery) { result in
            switch result {
            case .success(let graphQLResult):
                success(JSON(graphQLResult.data?.portfolio?.jsonObject as Any))
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    func getUserWatchlists(pageNo: Int = 1, pageSize: Int = 5, loader: Bool = false,success: @escaping SuccessJSONResponse, failure: @escaping FailureResponse) {
         
         let userWatchlistQuery = UserWatchlistQuery(pageNo: pageNo, pageSize: pageSize)
         Network.shared.apollo.fetch(query: userWatchlistQuery) { result in
             switch result {
             case .success(let graphQLResult):
                 success(JSON(graphQLResult.data?.watchlist?.jsonObject as Any))
             case .failure(let error):
                  failure(error)
             }
         }
     }
    
    func getUserBalance(loader: Bool = false,success: @escaping SuccessJSONResponse, failure: @escaping FailureResponse) {
        
        let userBalanceQuery = UserBalanceQuery()
        Network.shared.apollo.fetch(query: userBalanceQuery) { result in
            switch result {
            case .success(let graphQLResult):
                success(JSON(graphQLResult.data?.jsonObject as Any))
            case .failure(let error):
                failure(error)
            }
        }
    }
}
