//
//  SocketManager.swift
//  CelebrityTradingApp
//
//  Created by Vikash on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import Foundation
//import SocketIO
import SwiftyJSON
//import Starscream

enum EventListnerKeys : String {
    case didConnect = "connect"
    case didDisConnect = "disconnect"
    case handshakeWithServer = "handshakeWithServer"
    case eventAck = "eventAck"
    case syncStatus = "Sync.status"
    case shareLocation = "Sync.shareLocation"
    case updateCurrentLocation = "Sync.updateCurrentLocation"

    case authentication = "authentication"
    case authenticated = "authenticated"
    case unauthorized = "unauthorized"
    case connectionEstablished = "connection-established"
    case socketError = "socket-error"

    case subscribe = "subscribe"
    case market = "market"
}

//class SocketHelper: NSObject {
//
//    private var socket: SocketIOClient?
//    private var manager: SocketManager?
//    private var timer: Timer?
//    var didDisConnect: (() -> ())?
//    var didConnectSocket = false
//    private var socketRetryCounter = 0
//
//    private let token = "Bearer \("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtaWQiOiIxIiwiZWlkIjoiMSIsImVtYWlsIjoiam9obnNtaXRoQGV4YW1wbGUuY29tIiwiaXAiOiIxMDMuNTUuODkuNjQiLCJuYmYiOjE1ODc5NDc3NzQsImV4cCI6MTU4ODAzNDE3NCwiaWF0IjoxNTg3OTQ3Nzc0fQ.UiHQJYidFaT_vOCk2ECv2iBNXnQQYExNIzxRFsJDhhM")"
//
//    private var baseUrlForSocket : String {
//        return "ws://uoex-api.swayeformusic.com/"
//    }
//
//    //"https://CelebrityTradingAppsocket.appskeeper.com?authorization="
//
//    enum SocketCodes : Int {
//        case SocketSuccess = 200
//    }
//
//    static let shared = SocketHelper()
//
//    var websocket = WebSocket(url: URL(string: "wss://uoex-api.swayeformusic.com/")!)
//
//    fileprivate var socketHandlerArr = [((()->Void))]()
//    typealias ObjBlock = @convention(block) () -> ()
//
//    private override init() {
//        super.init()
//        //self.initializeSocket()
//        websocket.onText = { (textf) in
//            print("got some text: \(textf)")
//        }
//    }
//
//    public func connect() {
//
//        websocket.connect()
//        self.websocket.delegate = self
//        websocket.advancedDelegate = self
//
//    }
//
//    deinit {
//        websocket.disconnect(forceTimeout: 0)
//        websocket.delegate = nil
//    }
//
//    public func disConnect() {
//        websocket.disconnect(forceTimeout: 0)
//        websocket.delegate = nil
//    }
//
//    public func subsribeToMarket() {
//      //  websocket.ev
//    }
//
////    public func initializeSocket() {
////
////        self.manager = SocketManager(socketURL:  URL(string: baseUrlForSocket)!, config: [.log(true), .forceNew(true), .reconnectAttempts(10), .reconnectWait(6000), .forceWebsockets(true), .compress])
////
////        self.socket = manager?.defaultSocket
////    }
//
////    func connectSocket(handler:(()->Void)? = nil) {
////        self.socket?.connect()
////        if let status = self.socket?.status {
////            if status == .connected {
////                handler?()
////                return
////            }
////            else {
////                if let handlr = handler {
////                    if !self.socketHandlerArr.contains(where: { (handle) -> Bool in
////                        let obj1 = unsafeBitCast(handle as ObjBlock, to: AnyObject.self)
////                        let obj2 = unsafeBitCast(handlr as ObjBlock, to: AnyObject.self)
////                        return obj1 === obj2
////                    }) {
////                        self.socketHandlerArr.append(handlr)
////                    }
////                }
////
////                if self.socket?.status != .connecting {
////
////                    self.socket?.on(EventListnerKeys.didConnect.rawValue) {[weak self] data, ack in
////                        let params: JSONDictionary = ["authorization": AppUserDefaults.value(forKey: .authorization).stringValue]
////                        DispatchQueue.delay(1, closure: {
////                            guard let `self` = self else { return }
////                            if !self.didConnectSocket {
////                                self.didConnectSocket = true
////
////                                print("in connectioning with socket")
////                                self.emit(with: EventListnerKeys.authentication, params)
////                                self.emit(with: EventListnerKeys.connectionEstablished, params)
////                            }
////                        })
////                    }
////
////                    self.socket?.on(EventListnerKeys.authenticated.rawValue) {[weak self] data, ack in
////                        guard let `self` = self else {return}
////                        DispatchQueue.delay(1, closure: {
////                            for handler in self.socketHandlerArr {
////                                handler()
////                            }
////                            self.socketHandlerArr = []
////                        })
////                    }
////
////                    self.socket?.on(EventListnerKeys.didDisConnect.rawValue) { data, ack in
//////                        if !isLoggedOut {
////                            self.initializeSocket()
////                            self.didDisConnect?()
//////                        }
////                    }
////                    self.socket?.connect()
////                    self.socket?.on("statusChange") { data, ack in
////                        printDebug("Status Changed")
////                        printDebug("================")
////                        printDebug(data)
////                        printDebug("=================")
//////                        if isLoggedOut {
//////                            self.closeConnection()
//////                        }
////                    }
////
////                    self.socket?.on("socket-error", callback: { (data, ack) in
////                        printDebug("================")
////                        print("socket expired")
////                        printDebug("================")
////                        self.closeConnection()
////                        DispatchQueue.delay(2, closure: {
////                            self.initializeSocket()
////                            self.connectSocket()
////                        })
////                    })
////                }
////            }
////        }
////    }
//
//    func closeConnection() {
//        websocket.disconnect()
////        stopTimer()
//    }
//
//    private func stopTimer() {
//        timer?.invalidate()
//        timer = nil
//    }
//
////    private func reconnectOnTokenExpiry() {
////        on(with: .socketError) {[weak self] (data) in
////            self?.off(with: .socketError)
////            self?.websocket.connect()
////        }
////    }
//
////    //MARK:- Emit
////    func emit(with event: EventListnerKeys , _ params : JSONDictionary?, response: ((JSON) -> Void)? = nil) {
////        self.connectSocket(handler: {
////            self.emitWithACK(withTimeoutAfter: 20, event: event, params: params, array: nil)
////        })
////    }
////
////    func emitArray(with event: EventListnerKeys , _ array : [String]){
////        self.connectSocket(handler: {
////            self.emitWithACK(withTimeoutAfter: 20, event: event, params: nil, array: nil)
////        })
////    }
////
////    func on(with event: EventListnerKeys, completion: @escaping (([Any]) -> ())) {
////        SocketHelper.shared.socket?.on(event.rawValue, callback: { (data, ack) in
////            completion(data)
////        })
////    }
////
////    func emitNormal(with event: EventListnerKeys, params: JSONDictionary) {
////        socket?.emit(event.rawValue, params)
////    }
//
//    func off(with event: EventListnerKeys) {
//        SocketHelper.shared.socket?.off(event.rawValue)
//    }
//
//    private func emitWithACK(withTimeoutAfter seconds: Double, event: EventListnerKeys, params : JSONDictionary?, array:[Any]?) {
//        var ack : OnAckCallback? = nil
//        if let tempParams = params {
//            ack = self.socket?.emitWithAck(event.rawValue, tempParams)
//        } else if let tempArray = array {
//            ack = self.socket?.emitWithAck(event.rawValue, tempArray)
//        } else {
//            ack = self.socket?.emitWithAck(event.rawValue)
//        }
//
//        if event == .authentication {
//            socketRetryCounter += 1
//        }
//        if socketRetryCounter == 2 {
//            socketRetryCounter = 0
//        }
//
//        ack?.timingOut(after: seconds, callback: { (data) in
//        })
//    }
//}

//MARK:- Custom emit methods
//extension SocketHelper {
//
//    func subscribe(text: String) {
//        websocket.onText = { (textf) in
//            print("got some text: \(textf)")
//        }
//    }
//
//    func writeData(text: String) {
//        websocket.write(string: text)
//    }
//}


//extension SocketHelper: WebSocketDelegate, WebSocketAdvancedDelegate {
//
//    func websocketDidConnect(socket: WebSocket) {
//        print("connected")
//    }
//
//    func websocketDidDisconnect(socket: WebSocket, error: Error?) {
//        print("websocketDidDisconnect")
//    }
//
//    func websocketDidReceiveMessage(socket: WebSocket, text: String, response: WebSocket.WSResponse) {
//        print("message", text)
//    }
//
//    func websocketDidReceiveData(socket: WebSocket, data: Data, response: WebSocket.WSResponse) {
//        print(data)
//    }
//
//    func websocketHttpUpgrade(socket: WebSocket, request: String) {
//
//    }
//
//    func websocketHttpUpgrade(socket: WebSocket, response: String) {
//
//    }
//
//    func websocketDidConnect(socket: WebSocketClient) {
//        print("didconnect")
//    }
//
//    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
//        print("error", error?.localizedDescription ?? "default error")
//        SocketHelper.shared.connect()
//        let text = "{\"event\":\"subscribe\",\"topic\":\"market\",\"id\": 25647898}"
//
//        SocketHelper.shared.subscribe(text: text)
//        print("websocketDidDisconnect")
//    }
//
//    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
//        print("webtext", text)
//         print("websocketDidReceiveMessage")
//    }
//
//    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
//        print("webdata", data)
//         print("websocketDidReceiveData")
//    }
//
//}


