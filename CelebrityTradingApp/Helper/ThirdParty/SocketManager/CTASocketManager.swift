//
//  CTASocketManager.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 01/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import SwiftyJSON
import SwiftWebSocket

typealias CompletionBlock = (_ result: String,_ error: Bool, _ marketData:Bool) -> Void

class CTASocketManager: NSObject {
    
    internal var socketCompletion: CompletionBlock?
    
    static let sharedInstance = CTASocketManager()
    
    static let webSocketURL = "wss://uoex-api.swayeformusic.com/"
    let webSocketManager = WebSocket.init(webSocketURL)
    override init() {
        super.init()
    }
    
    func startWebSocket(_ completionBlock : CompletionBlock?) {
        //connection open handlet
        webSocketManager.event.open = {
            print("websocket opened")
            completionBlock?("websocket opened",false, false)
        }
        //connection closed handler
        webSocketManager.event.close = { code, reason, clean in
            print("websocket closed")
             completionBlock?("websocket closed",false, false)
        }
        webSocketManager.event.error = { error in
            print("websocket error :\(error)")
              completionBlock?("websocket error :\(error)",true, false)
        }
    
        webSocketManager.event.message = { message in
            if let text = message as? String {
                self.returnMarketData(data: text)
                completionBlock?(text,false, true)
            }
        }
    }
    
    func subscribeToMakret() {
        let stringToSend = "{\"event\":\"subscribe\",\"topic\":\"market\",\"id\": 25647898}"
        webSocketManager.send(text: stringToSend)
    }
    
    func unSubscribeToMakret() {
        let stringToSend = "{\"event\":\"unsubscribe\",\"topic\":\"market\",\"id\": 25647898}"
        webSocketManager.send(text: stringToSend)
    }
    
    func closeWebsocket() {
        webSocketManager.close()
    }

    private func returnMarketData(data: String) {
        guard let dataFromString = data.data(using: .utf8, allowLossyConversion: false) else {return}

        var swiftyJson:JSON!
        do {
            swiftyJson = try JSON(data: dataFromString)
        } catch {
            print("Error JSON: \(error)")
        }
        let marketDatas = MarketData.returnMarketDataListingArray(swiftyJson[ApiKey.data])
        if !marketDatas.isEmpty {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateMarketData"), object: self, userInfo: ["marketData": marketDatas])
        }
    }
}
