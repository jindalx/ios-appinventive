//
//  UINavigation+Extension.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 28/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

extension UINavigationBar {
    func installBlurEffect() {
        
        isTranslucent = true
        setBackgroundImage(UIImage(), for: .default)
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        
        var blurFrame = bounds
        blurFrame.size.height += statusBarHeight
        blurFrame.origin.y -= statusBarHeight
        let blurView  = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        blurView.isUserInteractionEnabled = false
        blurView.frame = blurFrame
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(blurView)
        blurView.layer.zPosition = -1
    }
    
  
}

