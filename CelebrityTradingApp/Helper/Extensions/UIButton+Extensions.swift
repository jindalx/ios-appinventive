//
//  UIButton+Extensions.swift
//  CelebrityTradingApp
//
//  Created by Vikash Sinha on 25/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    override var isSelected: Bool{
        didSet{
            let color = isSelected ? AppColors.appLightTeal : AppColors.clear
            self.backgroundColor = color
            self.setTitleColor(AppColors.appBlackColor, for: .normal)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
