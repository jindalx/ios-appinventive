//
//  UIColorExtensions.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 19/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(r: Int, g: Int, b: Int, alpha : CGFloat) {
        assert(r >= 0 && r <= 255, "Invalid red component")
        assert(g >= 0 && g <= 255, "Invalid green component")
        assert(b >= 0 && b <= 255, "Invalid blue component")
        assert(alpha >= 0 && alpha <= 1, "Invalid alpha component")
        
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
    
    //Returns the color based on the given R,G,B and alpha values
    class func colorRGB(r:Int, g:Int, b:Int, alpha:CGFloat = 1)->UIColor{
        return UIColor(r: r, g: g, b: b, alpha: alpha)
    }
    

    struct AppColor {
        static let changeBlack     = UIColor.colorRGB(r: 64, g: 64, b: 64)
        static let changeBlue      = UIColor.colorRGB(r: 33, g: 100, b: 171)
        static let changeGray      = UIColor.colorRGB(r: 151, g: 151, b: 151)
        static let changeBlueDark  = UIColor.colorRGB(r: 15, g: 76, b: 130)
        static let changeDarkTeal  = UIColor.colorRGB(r: 0, g: 127, b: 142)
        static let changeLightTeal = UIColor.colorRGB(r: 1, g: 183, b: 203)
        static let arrowsGreen     = UIColor.colorRGB(r: 135, g: 236, b: 18)
        static let rrowsGreen2     = UIColor.colorRGB(r: 70, g: 127, b: 6)
        static let arrowsRed       = UIColor.colorRGB(r: 255, g: 0, b: 68)
        static let ruleGrey        = UIColor.colorRGB(r: 237, g: 237, b: 237)
        static let backgroundGrey3 = UIColor.colorRGB(r: 245, g: 245, b: 245)
        static let profileBorderColor =  UIColor.colorRGB(r: 161, g: 189, b: 205)
        static let lightTealColor = UIColor.colorRGB(r: 103, g: 244, b: 255)
        static let navigationColor = UIColor.colorRGB(r: 245, g: 244, b: 244)
        static let progressBarShadow = UIColor.colorRGB(r: 0, g: 0, b: 0, alpha: 0.2)
        static let graphBackground = UIColor.colorRGB(r: 245, g: 245, b: 245, alpha: 1.0)
        
        static let otherUserProfileGradientColors = [UIColor.init(r: 0, g: 0, b: 0, alpha: 0.43).cgColor,UIColor.init(r: 52, g: 60, b: 82, alpha: 0.0).cgColor,UIColor.init(r: 46, g: 51, b: 73, alpha: 0.0).cgColor,UIColor.init(r: 14, g: 19, b: 22, alpha: 1.0).cgColor]
        static let artistHeaderGradientColors = [UIColor.init(r: 44, g: 175, b: 205, alpha: 1.0).cgColor,UIColor.init(r: 15, g: 76, b: 130, alpha: 1.0).cgColor]
        static let progressBarColors = [UIColor.init(r: 33, g: 100, b: 171, alpha: 1.0),UIColor.init(r: 1, g: 183, b: 203, alpha: 1.0)]
    }

}

