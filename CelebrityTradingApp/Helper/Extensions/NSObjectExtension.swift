//
//  NSObjectExtension.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 21/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

 extension NSObject{
    
    ///Retruns the name of the class
    class var className: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    ///Retruns the name of the class
    var className: String{
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}

extension NSLayoutConstraint {
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        newConstraint.isActive = self.isActive
        
        NSLayoutConstraint.deactivate([self])
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
