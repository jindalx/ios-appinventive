//
//  StringExtension.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit

extension String {
    
    //MARK: Returns a localized string
    var localized:String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    //MARK: sizeCount
    func sizeCount(withFont font: UIFont, boundingSize size: CGSize) -> CGSize {
        let mutableParagraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        mutableParagraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: mutableParagraphStyle]
        let tempStr = NSString(string: self)
        
        let rect: CGRect = tempStr.boundingRect(with: size, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
        let height = ceilf(Float(rect.size.height))
        let width = ceilf(Float(rect.size.width))
        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }
    
    func getFormattedDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Date.DateFormat.yyyy_MM_dd.rawValue
        let yourDate = dateFormatter.date(from: self)!
        dateFormatter.dateFormat = Date.DateFormat.eMMMd.rawValue
        return dateFormatter.string(from: yourDate)
    }
    
    func toDate(dateFormat:String = Date.DateFormat.yyyy_MM_dd.rawValue, timeZone:TimeZone = TimeZone.current)->Date? {
        let frmtr = DateFormatter()
        frmtr.dateFormat = dateFormat
        return frmtr.date(from: self)
    }
    
}

