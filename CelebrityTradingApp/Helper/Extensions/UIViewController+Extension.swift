
//
//  UIViewController+Extension.swift
//  MeetCue
//
//  Created by Vikash on 21/02/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

extension UIViewController {
    
    
    var topbarHeight: CGFloat {
        return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    //To present over context
    func presentOnRoot(with viewController : UIViewController){
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }

    //function to pop the target from navigation Stack
    func push(_ viewController: UIViewController, animated:Bool = true) {
        sharedAppDelegate.sharedNavController?.pushViewController(viewController, animated: animated)
    }
    
    func getParentViewController() -> UIViewController? {
        if let viewC = self.owningViewController() {
            if viewC.navigationController != nil {
                return viewC
            } else if let finalVC = viewC.owningViewController(), finalVC.navigationController != nil {
                return finalVC
            }
        }
        return nil
    }
    
    func showAlert( title : String = "", msg : String, affirmativeTitle : String = "Okay", showCancel : Bool = false,isCancelBtnAction: Bool = false,_ completion : (()->())? = nil) {
        
        let alertViewController = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title:
        affirmativeTitle, style: UIAlertAction.Style.default) { (action : UIAlertAction) -> Void in
            completion?()
        }
        
        alertViewController.addAction(okAction)
        if showCancel{
            alertViewController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action : UIAlertAction) -> Void in
                if isCancelBtnAction {
                    self.navigationController?.popViewController(animated: true)
                }
            }))
        }
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func registerForKeyboardWillShowNotification(_ scrollView: UIScrollView, usingBlock block: ((CGSize?) -> Void)? = nil) {
         _ = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil, using: { notification -> Void in
             let userInfo = notification.userInfo!
             let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
             let contentInsets = UIEdgeInsets(top: scrollView.contentInset.top, left: scrollView.contentInset.left, bottom: keyboardSize.height, right: scrollView.contentInset.right)

             scrollView.setContentInsetAndScrollIndicatorInsets(contentInsets)
             block?(keyboardSize)
         })
     }

     func registerForKeyboardWillHideNotification(_ scrollView: UIScrollView, usingBlock block: ((CGSize?) -> Void)? = nil) {
         _ = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil, using: { notification -> Void in
             let userInfo = notification.userInfo!
             let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
             let contentInsets = UIEdgeInsets(top: scrollView.contentInset.top, left: scrollView.contentInset.left, bottom: 0, right: scrollView.contentInset.right)

             scrollView.setContentInsetAndScrollIndicatorInsets(contentInsets)
             block?(keyboardSize)
         })
     }
    
}


extension UIResponder {
    func next<T: UIResponder>(_ type: T.Type) -> T? {
        return next as? T ?? next?.next(type)
    }
    
    func owningViewController() -> UIViewController? {
        var nextResponser = self
        while let next = nextResponser.next {
            nextResponser = next
            if let vc = nextResponser as? UIViewController {
                return vc
            }
        }
        return nil
    }
}


extension UIScrollView {

    func setContentInsetAndScrollIndicatorInsets(_ edgeInsets: UIEdgeInsets) {
        self.contentInset = edgeInsets
        self.scrollIndicatorInsets = edgeInsets
    }
}

