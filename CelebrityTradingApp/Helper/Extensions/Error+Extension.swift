//
//  Error+Extension.swift
//  CelebrityTradingApp
//
//  Created by Vishwesh on 04/05/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import Foundation

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}
