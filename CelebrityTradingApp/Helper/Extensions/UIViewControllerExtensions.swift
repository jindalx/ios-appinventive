//
//  UIViewControllerExtensions.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    class var storyboardID : String {
        
        return "\(self)"
    }
    
    //function to pop the target from navigation Stack
    @objc func pop(animated:Bool = true) {
        _ = self.navigationController?.popViewController(animated: animated)
    }
    
    //Use this function to pop to a specific view controller
    func popToSpecificViewController(atIndex index:Int, animated:Bool = true) {
          
          if let navVc = self.navigationController, navVc.viewControllers.count > index{
              _ = self.navigationController?.popToViewController(navVc.viewControllers[index], animated: animated)
          }
      }
 
    func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.windows.first!.rootViewController
        }
        
        if rootVC?.presentedViewController == nil {
            return rootVC
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(presented)
        }
        return nil
    }
    
    /// Start Loader
     func startLoader() {
         // TODO: Place Start Loader Code Here
     }
     
     /// Stop Loader
     func stopLoader() {
         // TODO: Place Stop Loader Code Here
     }
}


extension UINavigationController {
    func getPreviousViewController() -> UIViewController? {
        let count = viewControllers.count
        guard count > 1 else { return nil }
        let vc = viewControllers.last { (vc) -> Bool in
            vc.className == "ChatVC"
        }
        printDebug(vc?.className)
        return viewControllers[count - 2]
    }
}

var vSpinner : UIView?
 
extension UIViewController {
    func showSpinner() {
        let spinnerView = UIView.init(frame: self.view.bounds)
        spinnerView.backgroundColor = UIColor.clear
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.color = AppColors.appLightTeal
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            self.view.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}
