//
//  APIKeys.swift
//  CelebrityTradingApp
//
//  Created by Vikash on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import Foundation

enum ApiKey {
    static let name = "name"
    static let email = "email"
    static let password = "password"
    static let oldPassword = "oldPassword"
    static let deviceId = "deviceId"
    static let deviceType = "deviceType"
    static let deviceToken = "deviceToken"
    static let token = "token"
    static let platform = "platform"
    static let statusCode = "statusCode"
    static let statusType = "statusType"
    static let status = "status"
    static let code = "CODE"
    static let result = "result"
    static let results = "results"
    static let message = "message"
    static let Authorization = "authorization"
    static let userId = "user_id"
    static let socialId = "social_id"
    static let data = "data"
    static let contentType =  "Content-Type"
    static let ok = "OK"
    static let accessToken = "accessToken"
    static let bearerToken = "bearerToken"
    
    static let id = "id"
    static let symbol = "symbol"
    static let img = "img"
    static let tradeStats = "tradeStats"
    static let tradeSettings = "tradeSettings"
    static let recentNews = "recentNews"
    static let sparkline = "sparkline"
    static let buyEnabled = "buyEnabled"
    static let sellEnabled = "sellEnabled"
    static let artist = "artist"
    static let artists = "artists"
    static let list = "list"
    
    static let currency = "currency"
    static let balance = "balance"
    static let value = "value"
    
    static let date = "date"
    static let location = "location"
    static let url = "url"
    
    static let publishedTime = "publishedTime"
    static let content = "content"
    static let thumbnail = "thumbnail"
    static let postUrl = "postUrl"
    
    //Market data
    static let event = "event"
    static let topic = "topic"
    static let marketData = "marketData"
    static let Id = "Id"
    static let CoinName = "CoinName"
    static let CurrentTradingPrice = "CurrentTradingPrice"
    static let TotalVolume = "TotalVolume"
    static let ChangeInPrice = "ChangeInPrice"
    static let PreviousTradingPrice = "PreviousTradingPrice"
    static let MarketName = "MarketName"
    static let Rank = "Rank"
    static let Status = "Status"
    static let CoinFullName = "CoinFullName"
    static let TotalVolumeBaseCurrency = "TotalVolumeBaseCurrency"
    static let Last24HrsLow = "Last24HrsLow"
    static let Last24HrsHigh = "Last24HrsHigh"
    static let MinTradeAmount = "MinTradeAmount"
    static let MinTickSize = "MinTickSize"
    static let MinOrderValue = "MinOrderValue"
}

enum ApiState: String {
    case start, pending, complete, failed, completeWithNoData
    case noInternet = "No Internet"
}


enum LoadType {
    case terms
    case privacyPolicy
}

//MARK:- Api Code
//=======================
enum ApiCode {
    static var success: Int { return 200 } // Success
    static var unauthorizedRequest: Int { return 206 } // Unauthorized request
    static var headerMissing: Int { return 207 } // Header is missing
    static var phoneNumberAlreadyExist: Int { return 208 } // Phone number alredy exists
    static var requiredParametersMissing: Int { return 418 } // Required Parameter Missing or Invalid
    static var fileUploadFailed: Int { return 421 } // File Upload Failed
    static var pleaseTryAgain: Int { return 500 } // Please try again
    static var tokenExpired: Int { return 401 } // Token expired refresh token needed to be generated
}
