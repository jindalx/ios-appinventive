//
//  CommonFunctions.swift
//  CelebrityTradingApp
//
//  Created by Vikash on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit
import SwiftMessages
import MobileCoreServices
import CoreLocation

class CommonFunctions: NSObject {
 
    /// Show Toast With Message
    static func showToastWithMessage(msg: String, toastType: Theme = .error, completion: (() -> Swift.Void)? = nil) {

        DispatchQueue.mainQueueAsync {
            
            let warning = MessageView.viewFromNib(layout: .messageView)
            warning.configureTheme(toastType)
            warning.frame = CGRect(x: 0, y: 0, width: 20, height: 90)
            warning.layoutIfNeeded()
            warning.configureDropShadow()
            warning.configureContent(title: "", body: msg)
            warning.button?.isHidden = true
            var warningConfig = SwiftMessages.defaultConfig
            warning.bodyLabel?.textColor = .white
//            warning.bodyLabel?.font = AppFonts.segoeUIBold.withSize(15)
            warningConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: warningConfig, view: warning)
        }
    }

    // Native alert with one Button
    static func showAlert(_ title: String, _ msg : String, onVC: BaseVC? = nil, completion : (() -> Swift.Void)? = nil)
    {
        let alertViewController = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: LocalString.ok, style: UIAlertAction.Style.default) { (action : UIAlertAction) -> Void in
            completion?()
            alertViewController.dismiss(animated: true, completion: nil)
        }
        alertViewController.addAction(okAction)
        
        if let vc = onVC {
            vc.present(alertViewController, animated: true, completion: nil)
        } else {
            AppDelegate.shared.window?.rootViewController?.present(alertViewController, animated: true, completion: nil)
        }
        
    }
    
    // Native alert with two Buttons
    static func showAlertWithTwoBtns(_ title: String, _ msg : String, leftButtonText: String, rightButtonText: String, completionLeft : (() -> Swift.Void)? = nil, completionRight : (() -> Swift.Void)? = nil)
    {
        let alertViewController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let leftCompletion = UIAlertAction(title: leftButtonText, style: .cancel) { (action : UIAlertAction) -> Void in
            completionLeft?()
            alertViewController.dismiss(animated: true, completion: nil)
        }
        
        let rightCompletion = UIAlertAction(title: rightButtonText, style: .default) { (action : UIAlertAction) -> Void in
            completionRight?()
            alertViewController.dismiss(animated: true, completion: nil)
        }
        alertViewController.addAction(rightCompletion)
        alertViewController.addAction(leftCompletion)
        AppDelegate.shared.window?.rootViewController?.present(alertViewController, animated: true, completion: nil)
    }
    
    /// Delay Functions
    class func delay(delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure()
            
        }
    }
    
    class func getEstimatedLabelWidth(font: UIFont, text: String) -> CGFloat{
        let label = UILabel()
        label.font = font
        label.text = text
        return label.intrinsicContentSize.width
    }
    
    class func getEstimatedLabelHeight(font: UIFont, text: String = "A") -> CGFloat{
        let label = UILabel()
        label.font = font
        label.text = text
        return label.intrinsicContentSize.height
    }
    
    
    /// Show Action Sheet With Actions Array
    class func showActionSheetWithActionArray(_ title: String?, message: String?,
                                              viewController: UIViewController,
                                              alertActionArray : [UIAlertAction],
                                              preferredStyle: UIAlertController.Style)  {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        alertActionArray.forEach{ alert.addAction($0) }
        
        DispatchQueue.mainQueueAsync {
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc static func localFilePath(for url: URL) -> URL {
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    @objc static func saveFileToDocumentDirectory(_ sourceURL: URL, destinationURL: URL) -> Bool {
        try? FileManager.default.removeItem(at: destinationURL)
        do {
            try FileManager.default.copyItem(at: sourceURL, to: destinationURL)
            return true
        } catch let error {
            printDebug("Could not copy file to disk: \(error.localizedDescription)")
            return false
        }
    }
    
    @objc static func fileExistsLocally(for url: URL) -> Bool {
        if FileManager.default.fileExists(atPath: url.path) {
            return true
        } else {
            return false
        }
    }
    
    @objc static func removeFileFromLocalDirectoryIfExists(localURL: URL){
        try? FileManager.default.removeItem(at: localURL)
    }
  
    static func openBrowser(weblink: String){
        guard let url = URL(string: weblink) else { return }
        UIApplication.shared.open(url)

    }
    
    static func shareWithActivityViewController(VC:UIViewController) {
        let text = "Hey, check out the app CTA!"
        let shareAll = [text] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = VC.view
        VC.present(activityViewController, animated: true, completion: nil)
    }
    
    static func forceLogout(){
        AppUserDefaults.removeAllValues()
        AppUserDefaults.save(value: "", forKey: .authorization)
    }

}


