//
//  AppColor.swift
//  CelebrityTradingApp
//
//  Created by Vikash on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//

import UIKit

class AppColors : UIColor{
    
    private enum ColorName : String{
        
        case appLightTeal
        case appBlackColor
        case appWhiteFont243
        case appTextBlueColor35Disabled
        case textFieldUnderlineGrey
        case appRedColor
        case shadowColor
        case appDarkGreen
        case appPinkColor
        case backgroundPink
        case shadowColor197
        case appTextBlueColor35opacity78
        case graphHorizontal
        case graphVertical
        case appBlueDark
        
        var name : String{
            return self.rawValue
        }
    }
    
    
    static let appLightTeal = UIColor(named: ColorName.appLightTeal.name)!
    static let appBlackColor = UIColor(named: ColorName.appBlackColor.name)!
    static let appWhiteFont243 = UIColor(named: ColorName.appWhiteFont243.name)!
    static let appTextBlueColor35Disabled = UIColor(named: ColorName.appTextBlueColor35Disabled.name)!
    static let textFieldUnderlineGrey = UIColor(named: ColorName.textFieldUnderlineGrey.name)!
    static let appRedColor = UIColor(named: ColorName.appRedColor.name)!
    static let shadowColor = UIColor(named: ColorName.shadowColor.name)!
    static let appDarkGreen = UIColor(named: ColorName.appDarkGreen.name)!
    static let appPinkColor = UIColor(named: ColorName.appPinkColor.name)!
    static let backgroundPink = UIColor(named: ColorName.backgroundPink.name)!
    static let shadowColor197 = UIColor(named: ColorName.shadowColor197.name)!
    static let appTextBlueColor35opacity78 = UIColor(named: ColorName.appTextBlueColor35opacity78.name)!
   
    static let appBlueDark = UIColor(named: ColorName.appBlueDark.name)!
    static let graphHorizontalColor = UIColor(named: ColorName.graphHorizontal.name)!
    static let graphVerticalColor = UIColor(named: ColorName.graphVertical.name)!
    
}
