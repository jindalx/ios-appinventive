//
//  AppFonts.swift
//  CelebrityTradingApp
//
//  Created by Vikash on 21/04/20.
//  Copyright © 2020 CelebrityTradingApp. All rights reserved.
//
import UIKit

enum AppFonts : String {
    case sfRoundedHeavy = "SFProRounded-Heavy"
    case sfRoundedRegular = "SFProRounded-Regular"
    case sfRoundedSemiBold = "SFProRounded-Semibold"
    case sfRoundedMedium = "SFProRounded-Medium"
    case sfProDisplay = "SFProDisplay-Regular"
    case sfProDisplayLight = "SFProDisplay-Light"
    case sfProDisplayMedium = "SFProDisplay-Medium"
    case sfProDisplaySemiBold = "SFProDisplay-Semibold"
    case sfProDisplayHeavy = "SFProDisplay-Heavy"
    case sfProTextLight = "SFProText-Light"
    case sfProDisplayThinItalic = "SFProDisplay-ThinItalic"
}

extension AppFonts {
    
    func withSize(_ fontSize: CGFloat) -> UIFont {
        
        return UIFont(name: self.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    func withDefaultSize() -> UIFont {
        
        return UIFont(name: self.rawValue, size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
    }
    
}
