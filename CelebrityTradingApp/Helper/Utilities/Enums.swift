//
//  Enums.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation

enum AppStoryboard : String {
    case Main, Home, Account
}

enum NavigationController {
    case main, current
}

enum NavigationAction {
    case push, pop, present, dismiss
}

enum HomeScreeType {
    case Home, Swaye, Watchlist, ForYou
}

enum SlotType: Int {
    case hour = 0
    case week
    case month
    case twoMonth
    case threeMonth
    case yearly
}

var musicButtonType: MusicButtonType = .HOME

enum MusicButtonType {
    case HOME
    case DETAIL
}

enum HomeUserType {
      case swaye
      case watchList
}

enum ScreenToOpen: Int {
    case Summary = 0
    case Transfer = 1
    case Statement = 2
    
    init?(indexPath: NSIndexPath) {
        self.init(rawValue: indexPath.row)
    }
}

