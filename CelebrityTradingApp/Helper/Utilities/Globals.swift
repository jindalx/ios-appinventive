//
//  Globals.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 18/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import Foundation
import UIKit

/* Print Debug */
func printDebug<T>(_ obj : T) {
    #if DEBUG
    print(obj)
    #endif
}


//MARK: Delay Function
public func delay(_ delay:Double, closure:@escaping ()->()) {
    
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
        execute: closure
    )
}

let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate

let screen_size  = UIScreen.main.bounds.size
let screen_width  = UIScreen.main.bounds.size.width
let screen_height = UIScreen.main.bounds.size.height

let statusBarHeight: CGFloat = {
    var heightToReturn: CGFloat = 0.0
         for window in UIApplication.shared.windows {
             if let height = window.windowScene?.statusBarManager?.statusBarFrame.height, height > heightToReturn {
                 heightToReturn = height
             }
         }
    return heightToReturn
}()






