//
//  Router.swift
//  CelebrityTradingApp
//
//  Created by Vikash Kumar Chaubey on 23/04/20.
//  Copyright © 2020 Vikash Kumar Chaubey. All rights reserved.
//

import UIKit


protocol VCConfigurator {
    
    associatedtype RequiredParams
    func configureWithParameters(param : RequiredParams)
}

extension VCConfigurator {
    func configureWithParameters(param : ()){ }
}

class Router: NSObject {
    
    static let shared = Router()
    private override init() { }
    
    typealias FunctionType<Result: Any> = (() -> Result)?
    
    let mainNavigation: UINavigationController = {
        let navigation = UINavigationController()
        navigation.interactivePopGestureRecognizer?.isEnabled = true
        return navigation
    }()
    
    var currentTabNavigation: UINavigationController?
    var tabbar: TabBarVC?

}

extension Router {
        
    func navigate<T: VCConfigurator & UIViewController, U: Any>(to: T.Type,
                                                                storyboard: AppStoryboard?,
                                                                action: NavigationAction = .push,
                                                                animated: Bool = true,
                                                                navigationController: NavigationController = .current,
                                                                closure : FunctionType<U>) {
        switch action {
        case .push, .present:
            let toVC: T
            if let storyboard = storyboard {
                toVC = T.instantiate(fromAppStoryboard: storyboard)
            } else {
                toVC = T()
            }
            
            if let params = closure?() as? T.RequiredParams {
                toVC.configureWithParameters(param: params)
            }
            
            if navigationController == .main {
                if action == .push {
                    self.mainNavigation.pushViewController(toVC, animated: animated)
                } else {
                    self.mainNavigation.present(toVC, animated: animated, completion: nil)
                }
            } else {
                if action == .push {
                    self.currentTabNavigation?.pushViewController(toVC, animated: animated)
                } else {
                    self.currentTabNavigation?.present(toVC, animated: animated, completion: nil)
                }
            }
        case .pop, .dismiss:
            if navigationController == .main {
                guard let toVC = self.mainNavigation.viewControllers.last(where: {$0 is T}) else { return }
                if action == .pop {
                    self.mainNavigation.popToViewController(toVC, animated: animated)
                } else {
                    toVC.dismiss(animated: animated, completion: nil)
                }
            } else {
                guard let toVC = self.currentTabNavigation?.viewControllers.last(where: {$0 is T}) else { return }
                if action == .pop {
                    self.currentTabNavigation?.popToViewController(toVC, animated: animated)
                } else {
                    toVC.dismiss(animated: animated, completion: nil)
                }
            }
        }
    }
}

extension Router {
    //MARK: Open Buy and Trade Controller from the TabBarVC
    
}
